import openravepy as orpy
import numpy as np
import scipy as sp
from scipy.spatial import ConvexHull
from cdd import RepType
from cone import FractionToFloatArray
from copy import deepcopy
import cone
import cvxopt
import time

import hullobject
import trimesh
import utils

# Important notes: stability result differs by pretty much when changing this _decimals. Part of it
# may be because we are mainly using 'float' option when computing cdd (faster but less precise).
_decimals = 8 # for np.around


def GetContactPoints(robot, mobj, qgrasp, computeLocal=True, TcomLocal=None):
    """Return a list of 2 sublists. Each sublist contains contact points between one fingertip
    and the object. qgrasp is need to detemine the planes that contact points are lying
    on. The argument computeLocal indicates if the function should return contact points
    described in the glbal frame or the local frame.

    ** Local frame refers to a frame attached to the object's COM which might not
       necessarily coincide with the object's frame.

    Return lhullPoints, rhullPionts, Rl, Rr

    lhullPoints and rhullPoints are filtered contact points.

    Rl and Rr are rotation matrix describing the surfaces' frame --> the last column of
    each matrix represents a normal vector pointing out of the corresponding contact
    surface. The first two columns describe the remaining axes (parallel to the surface).

    Assumption:

    1. The robot must already be grabbing (in contact with) the object.

    2. robot is equipped with a parallel jaw gripper. Therefore, there # are only two
    parallel contact surfaces.
    """

    lfingertipname = 'robotiq_fingertip_l'
    rfingertipname = 'robotiq_fingertip_r'
    # lfingertipname = 'FingertipL'
    # rfingertipname = 'FingertipR'
    
    # If the robot is already grabbing the object, need to ungrab first.
    isGrabbing = robot.IsGrabbing(mobj) is not None
    if isGrabbing:
        robot.Release(mobj)

    # Change the collision checker to pqp in order to be able to obtain contact points.
    lcolRep = orpy.CollisionReport()
    rcolRep = orpy.CollisionReport()
    env = robot.GetEnv()
    prevChecker = env.GetCollisionChecker()
    usingNewChecker = not (prevChecker.GetXMLId() == 'pqp')
    if usingNewChecker:
        env.SetCollisionChecker(orpy.RaveCreateCollisionChecker(env, 'pqp'))

    env.CheckCollision(robot.GetLink(lfingertipname), mobj, report=lcolRep)
    env.CheckCollision(robot.GetLink(rfingertipname), mobj, report=rcolRep)
    if usingNewChecker:
        env.SetCollisionChecker(prevChecker)

    # Extract all detected contact points
    lpoints = [c.pos for c in lcolRep.contacts]
    rpoints = [c.pos for c in rcolRep.contacts]
    if len(lpoints) == 0:
        raise Exception("No contact detected on the left fingertip")
    if len(rpoints) == 0:
        raise Exception("No contact detected on the right fingertip")

    # Restore the grabbing state
    if isGrabbing:
        robot.Grab(mobj)

    # Preprocess contact points
    approachingDir, slidingDir, delta = qgrasp[-3:] # unpack data
    realApproachingDir = np.mod(approachingDir, 6)
    ibox = int(approachingDir/6)
    link = mobj.GetLinks()[ibox]
    extents = link.GetGeometries()[0].GetBoxExtents()
    # grippingDir is 
    if np.mod(realApproachingDir, 3) == 0:
        # Approaching direction is +X/-X
        if np.mod(slidingDir, 3) == 1:
            extent = extents[2]
            grippingDir = 2
        elif np.mod(slidingDir, 3) == 2:
            extent = extents[1]
            grippingDir = 1
    elif np.mod(realApproachingDir, 3) == 1:
        # Approaching direction is +Y/-Y
        if np.mod(slidingDir, 3) == 0:
            extent = extents[2]
            grippingDir = 2
        elif np.mod(slidingDir, 3) == 2:
            extent = extents[0]
            grippingDir = 0
    elif np.mod(realApproachingDir, 3) == 2:
        # Approaching direction is +Y/-Y
        if np.mod(slidingDir, 3) == 0:
            extent = extents[1]
            grippingDir = 1
        elif np.mod(slidingDir, 3) == 1:
            extent = extents[0]
            grippingDir = 0

    # Create a convex hull of all contact points (for removing redundant points). We need only the
    # contact points that are vertices of the contact surface.

    # We first transform all points into the link's frame because in the link's frame, there will be
    # one coordinate which is (approximately) constant.
    Tlink = link.GetTransform()
    TlinkInv = np.linalg.inv(Tlink)
    lLinkLocalPoints = [utils.Transform(p, TlinkInv) for p in lpoints]
    rLinkLocalPoints = [utils.Transform(p, TlinkInv) for p in rpoints]

    # All the points (in the link's local frame) are coplanar. The plane is actuall described by
    # grippingDir. Due to some numerical error, we compute the average of that *constant*
    # coordinate.    
    lAvgCoord = np.average([p[grippingDir] for p in lLinkLocalPoints])
    rAvgCoord = np.average([p[grippingDir] for p in rLinkLocalPoints])

    # Process the left contact surface
    lLinkLocalPoints2d = [np.asarray([p[i] for i in xrange(3) if not (i == grippingDir)]) for p in lLinkLocalPoints]
    lHull = ConvexHull(lLinkLocalPoints2d, qhull_options='E0.001')
    lHullLocalPoints2d = [lHull.points[i].tolist() for i in lHull.vertices] # a set of non-redundant points
    lHullLocalPoints = deepcopy(lHullLocalPoints2d)
    for point in lHullLocalPoints:
        point.insert(grippingDir, lAvgCoord)
    lHullLocalPoints = [np.asarray(p) for p in lHullLocalPoints]
    
    # Process the right contact surface
    rLinkLocalPoints2d = [np.asarray([p[i] for i in xrange(3) if not (i == grippingDir)]) for p in rLinkLocalPoints]
    rHull = ConvexHull(rLinkLocalPoints2d, qhull_options='E0.001')
    rHullLocalPoints2d = [rHull.points[i].tolist() for i in rHull.vertices] # a set of non-redundant points
    rHullLocalPoints = deepcopy(rHullLocalPoints2d)
    for point in rHullLocalPoints:
        point.insert(grippingDir, rAvgCoord)
    rHullLocalPoints = [np.asarray(p) for p in rHullLocalPoints]

    # Now transform all filtered points back into the global frame
    lPoints = [utils.Transform(p, Tlink) for p in lHullLocalPoints]
    rPoints = [utils.Transform(p, Tlink) for p in rHullLocalPoints]

    # Now we compute the description of the contact surface's frame (= obtain normal and tangential
    # components). We make use of the information of the gripper's transformation.
    Tmanip = robot.GetActiveManipulator().GetTransform()
    Rmanip = utils.ExtractRotationMatrix(Tmanip)

    # Rl is the rotation matrix describing the contact surface's frame of the surface touchuing the
    # gripper's left finger tip. The last column represent the normal vector of the surface's frame.
    # Since we are considering the free body diagram of the object, the normal vector should be
    # pointing *inward* (toward the object).

    # The following calculations are based on the current gripper configuration (denso + robotiq
    # gripper). If the robot changes, we also need to modify this.
    Rl = np.eye(3)
    Rl[0:3, 0] = Rmanip[0:3, 0]
    Rl[0:3, 1] = -Rmanip[0:3, 2]
    Rl[0:3, 2] = Rmanip[0:3, 1]

    Rr = np.eye(3)
    Rr[0:3, 0] = -Rmanip[0:3, 0]
    Rr[0:3, 1] = -Rmanip[0:3, 2]
    Rr[0:3, 2] = -Rmanip[0:3, 1]

    # Now lPoints, rPoints, Rl, Rr are all described in the global frame.
    if not computeLocal:
        return [lPoints, rPoints, Rl, Rr]

    # Here we need to return local descriptions of everything we have. Important note: the local
    # description is with respect to the frame attached to the COM (which might not coincide with
    # the object's local frame!!!)
    if TcomLocal is not None:
        Tcom = np.dot(mobj.GetTransform(), TcomLocal)
    else:
        Tcom = mobj.GetTransform()
    TcomInv = np.linalg.inv(Tcom)

    lLocalPoints = [utils.Transform(p, TcomInv) for p in lPoints]
    rLocalPoints = [utils.Transform(p, TcomInv) for p in rPoints]
    RlLocal = np.dot(utils.ExtractRotationMatrix(TcomInv), Rl)
    RrLocal = np.dot(utils.ExtractRotationMatrix(TcomInv), Rr)
    return [lLocalPoints, rLocalPoints, RlLocal, RrLocal]


def ComputeLocalGraspMatrix(localContactPoints):
    G = []
    for point in localContactPoints:
        G.append(np.vstack([np.eye(3), utils.ComputeSkewFromVect(point)]))
    return np.hstack(G)


def ComputeGraspMatrix(contactPoints):
    G = []
    for point in contactPoints:
        G.append(np.vstack([np.eye(3), utils.ComputeSkewFromVect(point)]))
    return np.hstack(G)


def ComputeFaceRepresentation(nContactPoints, mu, R, fmax=None):
    """Compute the matrix Usurf.

    mu is the friction coefficient of the contact surface

    R is a rotation matrix representing the surface's frame (in the object's frame). The
    last column of R is the normal vector pointing out of the object's surface

    fmax is the maximum gripping force that the gripper can exert.

    """
    normal = R[0:3, 2]
    t1 = R[0:3, 0]
    t2 = R[0:3, 1]

    Upoint_i = np.vstack([-normal,
                          -t1 - mu*normal,
                           t1 - mu*normal,
                          -t2 - mu*normal,
                           t2 - mu*normal])

    if fmax is not None:
        ULastRow = np.tile(normal, nContactPoints)

    U = [Upoint_i for _ in xrange(nContactPoints)]
    Upoint = sp.linalg.block_diag(*U)

    if fmax is not None:
        Upoint = np.vstack([Upoint, ULastRow])
        
    Upoint = np.around(Upoint, decimals=_decimals)

    if fmax is not None:
        F = np.zeros(5*nContactPoints + 1)
        F[-1] = fmax
    else:
        F = np.zeros(5*nContactPoints)
    return [Upoint, F]


def ComputeSpanRepresentation(robots, qgrasps, mobj, Tobj, mu, fmax, TcomLocal=None):
    """Given a set of robots, qgrasps, and the object, compute the matrices describing the
    span representation of WGI:
    
              WGI \in span(Astance*Vall)

    This function returns two lists: A_all and Vsurf_all. If there is no other contact
    points, Astance can be computed from Astance = hstack(A_all) and Vall can be computed
    from Vall = diag(Vsurf_all).

    1. A_all is a horizontal concatenation of A (representing relative transformations
    between the grapsed link and the COM).

    2. Vsurf_all is a list of Vsurf_i.

    This function will be used in functions for testing static equilibrium in different cases.

    Input arguments:
    
    robots: a list of robots
    
    qgrasps: a list of qgrasps. Each qgrasp is of the form qgrasp = [ibox, approachinDir,
             slidingDir, Delta].
    
    TcomLocal: a transformation matrix representing the relative transformation between
               the frame attached to the COM and the object's real local frame (which is
               normally attached to the first link).

    """
    # This matrix A will be put in Astance
    # A = np.eye(6)
    # if TcomLocal is not None:
    #     Tcom = np.dot(Tobj, TcomLocal)
    # else:
    #     Tcom = Tobj
    # R = utils.ExtractRotationMatrix(Tcom)
    # p = Tcom[0:3, 3]
    # p_hat = utils.ComputeSkewFromVect(p)
    # A[0:3, 0:3] = -R
    # A[3:6, 0:3] = -np.dot(p_hat, R)
    # A[3:6, 3:6] = -R
    A = ComputeA(Tobj, TcomLocal)

    Vsurf_all = []
    A_all = []
    for (robot, qgrasp) in zip(robots, qgrasps):
        lpoints, rpoints, Rl, Rr = GetContactPoints(robot, mobj, qgrasp, computeLocal=True, TcomLocal=TcomLocal)
        print "len(lpoints) = {0}; len(rpoints) = {1}".format(len(lpoints), len(rpoints))
        Gl = ComputeLocalGraspMatrix(lpoints) # grasp matrix for the left contact surface
        Gr = ComputeLocalGraspMatrix(rpoints) # grasp matrix for the right contact surface
        # A valid contact force, f_all, is satisfying Upoint*f_all <= F.
        Upointl, Fl = ComputeFaceRepresentation(len(lpoints), mu, Rl, fmax)
        # try:
        #     Vpointl_VREP = cone.FaceToSpan(Upointl, Fl, 'float')
        # except RuntimeError:
        #     Vpointl_VREP = cone.FaceToSpan(Upointl, Fl, 'fraction')
        #     Vpointl_VREP = cone.FractionToFloatArray(Vpointl_VREP)
        # Vpointl = cone.ExtractMatrix(Vpointl_VREP, RepType.GENERATOR)
        try:
            Vpointl = cone.FaceToSpan(Upointl, Fl, 'float')
        except RuntimeError:
            Vpointl = cone.FaceToSpan(Upointl, Fl, 'fraction')
        # import IPython; IPython.embed(header="After Face to Span")
        Vsurfl = np.dot(Gl, Vpointl)
        Vsurf_all.append(Vsurfl)
        A_all.append(A)

        Upointr, Fr = ComputeFaceRepresentation(len(rpoints), mu, Rr, fmax)
        # try:
        #     Vpointr_VREP = cone.FaceToSpan(Upointr, Fr, 'float')
        # except RuntimeError:
        #     Vpointr_VREP = cone.FaceToSpan(Upointr, Fr, 'fraction')
        #     Vpointr_VREP = cone.FractionToFloatArray(Vpointr_VREP)
        # Vpointr = cone.ExtractMatrix(Vpointr_VREP, RepType.GENERATOR)
        try:
            Vpointr = cone.FaceToSpan(Upointr, Fr, 'float')
        except RuntimeError:
            Vpointr = cone.FaceToSpan(Upointr, Fr, 'fraction')
        Vsurfr = np.dot(Gr, Vpointr)
        Vsurf_all.append(Vsurfr)
        A_all.append(A)

    return [A_all, Vsurf_all]


def ComputeA(Tobj, TcomLocal=None):
    A = np.eye(6)
    if TcomLocal is None:
        Tcom = Tobj
    else:
        Tcom = np.dot(Tobj, TcomLocal)
    R = utils.ExtractRotationMatrix(Tcom)
    p = Tcom[0:3, 3]
    p_hat = utils.ComputeSkewFromVect(p)
    A[0:3, 0:3] = -R
    A[3:6, 0:3] = -np.dot(p_hat, R)
    A[3:6, 3:6] = -R
    return A
    

def CheckStaticEquilibrium(robots, qgrasps, mobj, Tobj, Trest, fmax, mu, myObject, otherLocalContactPoints=[], checkSingleRobot=False):
    """Check statuc equilibrium by taking into account grasping contact points and placement
    contact points (if any).

    otherLocalContactPoints is a list of placement contact points described in the
    object's local frame.

    checkSingleRobot: if enabled, the function will recursively check if grasping the
    object by one of the robots can already maintain the static equilibrium.

    This function returns a dictionary. If checkSingleRobot is set to True, result.keys()
    contains each robot index as well as 'all'. Otherwise, there is only one key: 'all'.
    The corresponding value is a boolean indicating the static equilibrium checking
    result.
    """

    import time
    
    TcomLocal = myObject.Tcom
    Tcom = np.dot(Tobj, TcomLocal)
    p = Tcom[0:3, 3]

    # Calculate the object's mass
    env = robots[0].GetEnv()
    g = env.GetPhysicsEngine().GetGravity()
    m = 0
    for link in mobj.GetLinks():
        m += link.GetMass()

    WGI = np.hstack([m*g, m*np.cross(p, g)])

    A_all = dict()
    Vsurf_all = dict()
    if len(otherLocalContactPoints) > 0:
        R = np.array(Trest[0:3, 0:3]) # rotational component of frames attached to contact
                                      # points, expressed in the world's frame
        Trel = np.linalg.inv(np.dot(Tobj, Tcom))
        RLocal = np.dot(Trel[0:3, 0:3], R)
        
        G = ComputeLocalGraspMatrix(otherLocalContactPoints)
        Upoint, F = ComputeFaceRepresentation(len(otherLocalContactPoints), mu, RLocal, np.linalg.norm(m*g))
        ts = time.time()
        try:
            Vpoint = cone.FaceToSpan(Upoint, F, 'float')
        except RuntimeError:
            Vpoint = cone.FaceToSpan(Upoint, F, 'fraction')
        te = time.time()
        print "Computing Vpoint (face --> span): {0}".format(te - ts)
        
        Vsurf = np.dot(G, Vpoint)
        Vsurf_all['placement'] = [Vsurf]

        A = ComputeA(Tobj, Tcom)
        A_all['placement'] = [A]

    result = dict()
    for robotindex in xrange(len(robots)):
        A_all[robotindex], Vsurf_all[robotindex] = ComputeSpanRepresentation([robots[robotindex]], [qgrasps[robotindex]], mobj, Tobj, mu, fmax, TcomLocal)
        
        if checkSingleRobot:
            nsurfaces = len(A_all[robotindex]) + len(A_all['placement'])

            Astance = np.hstack(A_all[robotindex] + A_all['placement'])
            Vall = sp.linalg.block_diag(*(Vsurf_all[robotindex] + Vsurf_all['placement']))

            AstanceVall = np.dot(Astance, Vall)
            tobedeleted = []
            for icol in xrange(AstanceVall.shape[1]):
                if _Norm(AstanceVall[:, icol]) < 1e-6:
                    tobedeleted.append(icol)
            for icol in tobedeleted[::-1]:
                AstanceVall = np.delete(AstanceVall, icol, axis=1)
            # AstanceVall = np.around(AstanceVall, decimals=_decimals)
            # import IPython; IPython.embed()
            AstanceVall = RemoveRedundantColumns(AstanceVall)
            # print "\nnp.{0}\n".format(repr(AstanceVall))
            ts = time.time()
            try:
                Ustance, B = cone.SpanToFace(AstanceVall*nsurfaces, datatype=1, numtype='float')
            except RuntimeError:
                Ustance, B = cone.SpanToFace(AstanceVall*nsurfaces, datatype=1, numtype='fraction')
                # import IPython; IPython.embed()
            te = time.time()
            print "Computing (Ustance, B) (span --> face) for robot {0}: {1}".format(robotindex, te - ts)
                
            result[robotindex] = np.alltrue(np.dot(Ustance, WGI) <= B)
        else:
            result[robotindex] = False    

    if checkSingleRobot:
        # If checkSingleRobot, we don't check the case when all robots are holding the
        # object at the same time.
        return result
            
    nsurfaces = len(A_all[0]) + len(A_all[1]) + len(A_all['placement'])
    Astance = np.hstack(A_all[0] + A_all[1] + A_all['placement'])
    Vall = sp.linalg.block_diag(*(Vsurf_all[0] + Vsurf_all[1] + Vsurf_all['placement']))
    AstanceVall = np.dot(Astance, Vall)
    
    # AstanceVall = np.around(AstanceVall, decimals=_decimals)
    tobedeleted = []
    for icol in xrange(AstanceVall.shape[1]):
        if _Norm(AstanceVall[:, icol]) < 1e-6:
            tobedeleted.append(icol)
    for icol in tobedeleted[::-1]:
        AstanceVall = np.delete(AstanceVall, icol, axis=1)
        # import IPython; IPython.embed()
    AstanceVall = RemoveRedundantColumns(AstanceVall)
    ts = time.time()
    try:
        Ustance, B = cone.SpanToFace(AstanceVall*nsurfaces, datatype=1, numtype='float')
    except RuntimeError:
        Ustance, B = cone.SpanToFace(AstanceVall*nsurfaces, datatype=1, numtype='fraction')
        # import IPython; IPython.embed()
    te = time.time()
    print "Computing (Ustance, B) (span --> face) 'all': {0}".format(te - ts)
                
    result['all'] = np.alltrue(np.dot(Ustance, WGI) <= B)
    return result

##########################################################################################
import cvxopt
import cvxopt.solvers
from cvxopt import matrix as cvxmat
from warnings import warn
try:
    import cvxopt.glpk
    __default_solver = 'glpk'
    # GLPK is the fastest LP solver I could find so far:
    # <https://scaron.info/blog/linear-programming-in-python-with-cvxopt.html>
    # ... however, it's verbose by default, so tell it to STFU:
    cvxopt.solvers.options['glpk'] = {'msg_lev': 'GLP_MSG_OFF'}  # cvxopt 1.1.8
    cvxopt.solvers.options['msg_lev'] = 'GLP_MSG_OFF'  # cvxopt 1.1.7
    cvxopt.solvers.options['LPX_K_MSGLEV'] = 0  # previous versions
except ImportError:
    __default_solver = None
solve_qp = None
cvxopt.solvers.options['show_progress'] = False  # disable cvxopt output

from cvxopt.solvers import qp as cvxopt_qp
def cvxopt_solve_qp(P, q, G, h, A=None, b=None, initvals=None):
    """
    Solve a Quadratic Program defined as:

        minimize
            (1/2) * x.T * P * x + q.T * x

        subject to
            G * x <= h
            A * x == b  (optional)

    using CVXOPT
    <http://cvxopt.org/userguide/coneprog.html#quadratic-programming>.
    """
    # CVXOPT only considers the lower entries of P so we project on its
    # symmetric part beforehand
    P = .5 * (P + P.T)
    args = [cvxmat(P), cvxmat(q), cvxmat(G), cvxmat(h)]
    if A is not None:
        args.extend([cvxmat(A), cvxmat(b)])
    sol = cvxopt_qp(*args, initvals=initvals)
    if not ('optimal' in sol['status']):
        warn("QP optimum not found: %s" % sol['status'])
        return None
    return np.array(sol['x']).reshape((P.shape[1],))

if solve_qp is None:
    solve_qp = cvxopt_solve_qp

    
def _Norm(vect):
    return np.sqrt(np.dot(vect, vect))


def IsPositiveCombination(b, A):
    """
    Check if b can be written as a positive combination of lines from A.

    INPUT:

    - ``b`` -- test vector
    - ``A`` -- matrix of line vectors to combine

    OUTPUT:

    True if and only if b = A * x for some x >= 0.
    """
    m = A.shape[1]
    P = np.eye(m)
    q = np.zeros(m)
    G = -np.eye(m)
    h = np.zeros(m)
    x = solve_qp(P, q, G, h, A, b)
    if x is None:
        return False
    else:
        return _Norm(np.dot(A, x) - b) < 1e-10 and min(x) > -1e-10
    

def RemoveRedundantColumns(A):
    ts = time.time()
    case = 2
    if case == 0:
        return A
    elif case == 1:
        all_columns = set(range(A.shape[1]))
        tobedeleted = []
        for i in all_columns:
            if IsPositiveCombination(A[:, i], A[:, list(all_columns - set([i] + tobedeleted))]):
                tobedeleted.append(i)

        # tobedeleted = [i for i in all_columns if IsPositiveCombination(A[:, i], A[:, list(all_columns - set([i]))])]
        Anew = np.array(A)
        for index in tobedeleted[::-1]:
            Anew = np.delete(Anew, index, axis=1)
        te = time.time()
        print "RemoveRedundantColumns: execution time = {0} sec.".format(te - ts)
        return Anew
    else:
        V = [np.array(A[:, i]) for i in xrange(A.shape[1])]
        hull = ConvexHull(V, qhull_options='E0.001')
        print "RemoveRedundanteColumns will remove {0} columns".format(len(hull.points) - len(hull.vertices))
        Vnew = [hull.points[i] for i in hull.vertices]
        Anew = np.vstack(Vnew).T
        te = time.time()
        print "RemoveRedundantColumns: execution time = {0} sec.".format(te - ts)
        return Anew
        

##########################################################################################

def CheckStaticEquilibrium_LP(robots, qgrasps, mobj, Tobj, Trest, fmax, mu, myObject, otherContactPoints=[], checkSingleRobot=False):
    """
    """
    TcomLocal = myObject.Tcom
    Tcom = np.dot(Tobj, TcomLocal)
    pcom = Tcom[0:3, 3]

    # Calculate the object's mass
    env = robots[0].GetEnv()
    g = env.GetPhysicsEngine().GetGravity()
    m = 0
    for link in mobj.GetLinks():
        m += link.GetMass()

    WGI = -np.hstack([m*g, m*np.cross(pcom, g)])

    Usurf_all = []
    b_all = []
    G_all = []
    # Deal with contact points from grasps
    for (irobot, (robot, qgrasp)) in enumerate(zip(robots, qgrasps)):
        lPoints, rPoints, Rl, Rr = GetContactPoints(robot, mobj, qgrasp, computeLocal=False, TcomLocal=TcomLocal)
        G_l = ComputeGraspMatrix(lPoints)
        G_r = ComputeGraspMatrix(rPoints)
        G_all.append(G_l)
        G_all.append(G_r)
        Usurf_l, b_l = ComputeFaceRepresentation(len(lPoints), mu, Rl, fmax)
        Usurf_r, b_r = ComputeFaceRepresentation(len(rPoints), mu, Rr, fmax)
        Usurf_all.append(Usurf_l)
        Usurf_all.append(Usurf_r)
        b_all.append(b_l)
        b_all.append(b_r)

    # Deal with placement contact points
    R_other = np.array(Trest[0:3, 0:3])  # rotational component of frames attached to
                                         # contact points, expressed in the world's frame
    G_other = ComputeGraspMatrix(otherContactPoints)
    G_all.append(G_other)
    Usurf_other, b_other = ComputeFaceRepresentation(len(otherContactPoints), mu, R_other, fmax=None)
    
    Usurf_all.append(Usurf_other)
    b_all.append(b_other)

    # Prepare for solving an LP
    G = np.hstack(G_all)
    U = sp.linalg.block_diag(*Usurf_all)
    B = np.hstack(b_all)
    c = np.zeros(U.shape[1])
    # import IPython; IPython.embed()
    sol = cvxopt.solvers.lp(cvxmat(c), cvxmat(U), cvxmat(B), cvxmat(G), cvxmat(WGI))
    if not ('optimal' in sol['status']):
        return False, sol
    else:
        return True, sol

    
