import numpy as np
from .grasp import *
import random
import time
import openravepy as orpy
_rng = random.SystemRandom()

def ComputeTRot(axis, theta):
    if axis > mZ:
        raise ValueError("Wrong axis of rotation given")
    elif axis > pZ:
        axis = np.mod(axis, 3)
        theta *= -1.0
        
    c = np.cos(theta)
    s = np.sin(theta)
    if axis == pX:
        T = np.array([[ 1,  0,  0,  0], 
                      [ 0,  c, -s,  0], 
                      [ 0,  s,  c,  0] ,
                      [ 0,  0,  0,  1]], dtype=float)
    elif axis == pY:
        T = np.array([[ c,  0,  s,  0], 
                      [ 0,  1,  0,  0], 
                      [-s,  0,  c,  0] ,
                      [ 0,  0,  0,  1]], dtype=float)
    else:
        T = np.array([[ c, -s,  0,  0], 
                      [ s,  c,  0,  0], 
                      [ 0,  0,  1,  0] ,
                      [ 0,  0,  0,  1]], dtype=float)
    return T


def ComputeTTrans(axis, d):
    if axis > mZ:
        raise ValueError("Wrong axis of rotation given")
    elif axis > pZ:
        axis = np.mod(axis, 3)
        d *= -1.0

    T = np.eye(4)
    T[axis, 3] = d
    return T


def ComputeTGripper(Tlink, qgrasp, extent, unitScale=False):
    """Compute a transformation matrix for the gripper grasping the link at Tlink, given
    qgrasp in the form
    
               qgrasp = [..., approachingDir, slidingDir, delta]

    Convention: 
    - Approaching direction and sliding direction are specified in the object's local
      frame
    - Approaching vector is aligned with the gripper's +Z
    - Sliding direction is aligned with the gripper's +X
    - The gripper can slide along the sliding axis within the range [-delta, delta]. 
    - Delta is expressed in the link's local frame.
    - In case the object is composed of many boxes, approachingDir may be greater than
      6. That means the approaching direction is np.mod(approachingDir) of iBox =
      approachingDir/6.

    """
    approachingDir, slidingDir, _delta = qgrasp[-3:]
    if slidingDir > 2:
        delta = -_delta
    else:
        delta = _delta
        
    realApproachingDir = np.mod(approachingDir, 6)
    dx, dy, dz = extent

    if (realApproachingDir == pX):
        T0 = ComputeTRot(pY, 0.5*np.pi)
        if (slidingDir == pZ):
            T1 = ComputeTRot(pZ, np.pi)
        elif (slidingDir == pY):
            T1 = ComputeTRot(pZ, 0.5*np.pi)
        elif (slidingDir == mZ):
            T1 = np.eye(4)
        elif (slidingDir == mY):
            T1 = ComputeTRot(pZ, -0.5*np.pi)
    elif (realApproachingDir == mX):
        T0 = ComputeTRot(pY, -0.5*np.pi)
        if (slidingDir == pZ):
            T1 = np.eye(4)
        elif (slidingDir == pY):
            T1 = ComputeTRot(pZ, -0.5*np.pi)
        elif (slidingDir == mZ):
            T1 = ComputeTRot(pZ, np.pi)
        elif (slidingDir == mY):
            T1 = ComputeTRot(pZ, 0.5*np.pi)
    elif (realApproachingDir == pY):
        T0 = ComputeTRot(pX, -0.5*np.pi)
        if (slidingDir == pX):
            T1 = np.eye(4)
        elif (slidingDir == pZ):
            T1 = ComputeTRot(pZ, -0.5*np.pi)
        elif (slidingDir == mX):
            T1 = ComputeTRot(pZ, np.pi)
        elif (slidingDir == mZ):
            T1 = ComputeTRot(pZ, 0.5*np.pi)
    elif (realApproachingDir == mY):
        T0 = ComputeTRot(pX, 0.5*np.pi)
        if (slidingDir == pX):
            T1 = np.eye(4)
        elif (slidingDir == pZ):
            T1 = ComputeTRot(pZ, 0.5*np.pi)
        elif (slidingDir == mX):
            T1 = ComputeTRot(pZ, np.pi)
        elif (slidindDir == mZ):
            T1 = ComputeTRot(pZ, -0.5*np.pi)
    elif (realApproachingDir == pZ):
        T0 = np.eye(4)
        if (slidingDir == pX):
            T1 = np.eye(4)
        elif (slidingDir == pY):
            T1 = ComputeTRot(pZ, 0.5*np.pi)
        elif (slidingDir == mX):
            T1 = ComputeTRot(pZ, np.pi)
        elif (slidingDir == mY):
            T1 = ComputeTRot(pZ, -0.5*np.pi)
    else:
        T0 = ComputeTRot(pX, np.pi)
        if (slidingDir == pX):
            T1 = np.eye(4)
        elif (slidingDir == pY):
            T1 = ComputeTRot(pZ, -0.5*np.pi)
        elif (slidingDir == mX):
            T1 = ComputeTRot(pZ, np.pi)
        elif (slidingDir == mY):
            T1 = ComputeTRot(pZ, 0.5*np.pi)

    d = extent[np.mod(slidingDir, 3)]
    offset = 0.004
    if realApproachingDir > pZ:
        T2 = ComputeTTrans(pZ, -extent[realApproachingDir - 3] + offset)
    else:
        T2 = ComputeTTrans(pZ, -extent[realApproachingDir] + offset)
            
    if unitScale:
        T3 = ComputeTTrans(pX, delta*d)
    else:
        T3 = ComputeTTrans(pX, delta)
    
    return reduce(np.dot, [Tlink, T0, T1, T2, T3])


def ComputeTGripper2(mobj, linkindex, qgrasp, unitScale=False):
    approachingDir, slidingDir, delta = qgrasp[-3:]
    link = mobj.GetLinks()[linkindex]
    extents = link.GetGeometries()[0].GetBoxExtents()
    return ComputeTGripper(link.GetTransform(), qgrasp, extents, unitScale)
    

def GetPerpendicularVector(v):
    # for two vectors (x, y, z) and (a, b, c) to be perpendicular, the following
    # equation has to be fulfilled
    #     0 = ax + by + cz
    if (not (len(v) == 3)):
        raise ValueError('Dimension not compatible')    
    
    # x = y = z = 0 is not an acceptable solution
    if v[0] == v[1] == v[2] == 0:
        raise ValueError('Zero-vector')

    # If one dimension is zero, this can be solved by setting that to non-zero and
    # the others to zero. Example: (4, 2, 0) lies in the x-y-Plane, so (0, 0, 1) is
    # orthogonal to the plane.
    if v[0] == 0:
        return np.array([1., 0., 0.])
    if v[1] == 0:
        return np.array([0., 1., 0.])
    if v[2] == 0:
        return np.array([0., 0., 1.])

    # arbitrarily set a = b = 1 then the equation simplifies to
    #     c = -(x + y)/z
    c = -(v[0] + v[1])/float(v[2])
    d = 1./np.sqrt(2 + abs(c)**2)
    return np.array([d, d, d*c])


def Project(vect, option):
    """Project vect onto the specified plane indicated by option:
    0: Y-Z plane
    1: X-Z plane
    2: X-Y plane
    """
    projectedVect = np.array(vect)
    projectedVect[option] = 0
    return projectedVect


def Rotate(vect, T):
    """Rotate a 3D vector vect by the rotational part of T.
    """
    return np.dot(T[0:3, 0:3], vect)


def Transform(vect, T):
    return np.dot(T, np.append(vect, 1))[0:3]


def Norm(vect):
    """Return the L2-norm of the given vector. Faster than numpy.linalg.norm.
    """
    return np.sqrt(np.dot(vect, vect))


def Normalize(vect):
    norm = Norm(vect)
    if norm == 0:
        return vect
    else:
        return vect / norm


def ExtractRotation(T):
    """Return a transformation matrix Tnew containing only the rotational part of T.
    """
    Tnew = np.eye(4)
    Tnew[0:3, 0:3] = np.array(T[0:3, 0:3])
    return Tnew


def ExtractRotationMatrix(T):
    R = np.eye(3)
    R[0:3, 0:3] = T[0:3, 0:3]
    return R


def ExtractTranslation(T):
    """Return a transformation matrix Tnew containing only the translation part of T.
    """
    Tnew = np.eye(4)
    Tnew[0:3, 3] = np.array(T[0:3, 3])
    return Tnew


def ExtractTranslationVector(T):
    p = np.zeros(3)
    p = T[0:3, 3]
    return p


def CombineRotationTranslation(R, p):
    return np.vstack([np.hstack([R, p.reshape((3, 1))]), np.array([0, 0, 0, 1])])


def CreateTransformFromRotation(R):
    T = np.eye(4)
    T[0:3, 0:3] = np.array(R)
    return T


def CreateTransformFromTranslation(p):
    T = np.eye(4)
    T[0:3, 3] = np.array(p)
    return T


def WeightedChoice(choices):
    total = sum(w for c, w in choices)
    r = _rng.uniform(0, total)
    upto = 0
    for c, w in choices:
        upto += w
        if upto >= r:
            return c
    assert False


def WeightedChoice2(choices):
    total = sum(w for c, w in choices)
    s = _rng.uniform(0, total)
    upto = 0
    for c, w in choices:
        upto += w
        if upto >= s:
            return c + (upto - s)
    assert False


##########################################################################################
# Matrix manipulation    
    
def ComputeSkewFromVect(vect):
    S = np.array([[0, -vect[2], vect[1]],
                  [vect[2], 0, -vect[0]],
                  [-vect[1], vect[0], 0]])
    return S


def ComputeVectFromSkew(skew):
    S = np.array([skew[2, 1], skew[0, 2], skew[1, 0]])
    return S


_epsilon = 1e-10
def ComputeExponentMatrix(r):
    nr = Norm(r)
    if (nr <= _epsilon):
        return np.eye(3)
    R = ComputeSkewFromVect(r)
    return np.eye(3) + (R*np.sin(nr) + np.dot(R, R)*(1 - np.cos(nr))/nr)/nr


def ComputeLogVector(R):
    tR = np.trace(R)
    if (abs(tR + 1) > _epsilon):
        if (Norm(R - np.eye(3)) <= _epsilon):
            return np.zeros(3)
        else:
            phi = np.arccos(0.5*(tR - 1))
            return ComputeVectFromSkew((R - R.T)*phi*0.5/np.sin(phi))
    else:
        eigVals, eigVects = np.linalg.eig(R)
        for (i, val) in enumerate(eigVals):
            if abs(val - 1) <= _epsilon:
                return np.pi*np.real(eigVects[:, i])
        assert(False)

        
##########################################################################################

def CreateReferenceFrameKinBody(env, T=None, h=0.1, r=0.003):
    xmlData ="""
<Kinbody name="refframe">
  <body type="static" enable="false">
    <geom type="cylinder">
      <radius>{2}</radius>
      <height>{0}</height>
      <rotationaxis>0 0 1 90</rotationaxis>
      <translation>{1} 0 0</translation>
      <diffusecolor>1 0 0</diffusecolor>
    </geom>

    <geom type="cylinder">
      <radius>{2}</radius>
      <height>{0}</height>
      <translation>0 {1} 0</translation>
      <diffusecolor>0 1 0</diffusecolor>
    </geom>

    <geom type="cylinder">
      <radius>{2}</radius>
      <height>{0}</height>
      <rotationaxis>1 0 0 90</rotationaxis>
      <translation>0 0 {1}</translation>
      <diffusecolor>0 0 1</diffusecolor>
    </geom>
  </body>
</Kinbody>
""".format(h, 0.5*h, r)
    frame = env.ReadKinBodyXMLData(xmlData)
    if T is None:
        T = np.eye(4)
    frame.SetTransform(T)
    env.Add(frame, True)
    return frame


def AttachFrameToBody(frame, body, Trel, sleepTime=0.1):
    """Attach a ref. frame kinbody to the given *body*. Trel is the transformation of the
    frame in the body' local frame. The transformation of the frame is updated every
    sleepTime second.

    To use this function, do:

        frame = utils.CreateReferenceFrameKinBody(env, np.eye(4))
        thread = threading.Thread(target=AttachFrameToBody, args=(frame, mobj, myObject.Tcom))
        thread.start()

    """
    while True:
        frame.SetTransform(np.dot(body.GetTransform(), Trel))
        time.sleep(sleepTime)


def VisualizeFacets(myObject,  offset=[], startThreads=True):
    mobj = myObject.mobj
    env = mobj.GetEnv()
    Tobj_com = np.dot(mobj.GetTransform(), myObject.Tcom)

    facets = []
    for ifacet in myObject.facets.keys():
        faces = []
        uniqueIndices = []
        for iface in myObject.facets[ifacet]:
            faces.append(myObject.faces[iface])
            uniqueIndices += myObject.faces[iface].tolist()
        uniqueIndices = list(set(uniqueIndices))
        vertices = [myObject.vertices[i] for i in uniqueIndices]
        vTransformed = [Transform(v, Tobj_com) for v in vertices]

        # Reindex the indices
        newIndices = []
        for face in faces:
            newFace = [uniqueIndices.index(index) for index in face]
            newIndices.append(newFace)
            
        facet_trimesh = orpy.TriMesh(vTransformed, newIndices)
        facet = orpy.RaveCreateKinBody(env, '')
        facet.InitFromTrimesh(facet_trimesh)
        facet.SetName('facet{0:02d}'.format(ifacet))
        color = np.asarray([_rng.random() for _ in xrange(3)])
        for l in facet.GetLinks():
            for g in l.GetGeometries():
                g.SetDiffuseColor(color)
        env.Add(facet)
        facets.append(facet)

    # Create a frame which acts as a steering wheel for all the facets
    if len(offset) == 0:
        offset = np.array([0.35, 0, 0])
    Tframe = Tobj_com
    for i in xrange(3):
        Tframe = np.dot(Tframe, ComputeTTrans(i, offset[i]))
    frame = CreateReferenceFrameKinBody(env, T=Tframe)

    # Create threads so as to update all facets' transformation according to the frame
    import threading
    threads = []
    TframeInv = np.linalg.inv(Tframe)
    for facet in facets:
        thread = threading.Thread(target=AttachFrameToBody, args=[facet, frame, np.dot(TframeInv, facet.GetTransform())])
        if startThreads:
            thread.start()
        threads.append(thread)
    
    return facets, threads        
        

##########################################################################################
colors = dict()
colors['black'] = 0
colors['red'] = 1
colors['green'] = 2
colors['yellow'] = 3
colors['blue'] = 4
colors['magenta'] = 5
colors['cyan'] = 6
colors['white'] = 7
def Colorize(msg, color='white', bold=True):
    newmsg = '\033[{0};3{1}m{2}\033[0m'.format(int(bold), colors[color], msg)
    return newmsg

##########################################################################################
import cvxopt
import cvxopt.solvers
from cvxopt import matrix as cvxmat
from cvxopt.solvers import qp as cvxopt_qp

def solve_qp(P, q, G, h, A=None, b=None, initvals=None):
    """ (From Stephane's code: pymanoid lib)
        Solve a Quadratic Program defined as:
            minimize
                (1/2) * x.T * P * x + q.T * x
            subject to
                G * x <= h
                A * x == b  (optional)
        using CVXOPT
        <http://cvxopt.org/userguide/coneprog.html#quadratic-programming>.
        """
    # CVXOPT only considers the lower entries of P so we project on its
    # symmetric part beforehand
    P = .5 * (P + P.T)
    args = [cvxmat(P), cvxmat(q), cvxmat(G), cvxmat(h)]
    if A is not None:
        args.extend([cvxmat(A), cvxmat(b)])
    sol = cvxopt_qp(*args, initvals=initvals)
    if not ('optimal' in sol['status']):
        # log.warn("QP optimum not found: %s" % sol['status'])
        return None
    return np.array(sol['x']).reshape((P.shape[1],))


def norm(vect):
    return np.sqrt(np.dot(vect, vect))


def is_positive_column_combination(b, A):
    """test if b can be written as a positive linear combination of columns of A.

    solve minimize np.dot(x, x)
          subject to x >= 0,
                     Ax = b
    """
    m = A.shape[1] # dimension of x
    P = np.eye(m)
    q = np.zeros(m)
    G = -np.eye(m)
    h = np.zeros(m)
    x = solve_qp(P, q, G, h, A, b)
    if x is None:
        return False
    return norm(np.dot(A, x) - b) <= 1e-10 and min(x) > 1e-10


def remove_redundant_column(A):
    toberemoved = []
    all_columns = set(range(A.shape[1]))
    for icolumn in xrange(A.shape[1]):
        if is_positive_column_combination(A[:, icolumn], A[:, list(all_columns - set(toberemoved + [icolumn]))]):
            toberemoved.append(icolumn)

    Anew = np.array(A)
    for icolumn in toberemoved[::-1]:
        Anew = np.delete(Anew, icolumn, 1)
    return Anew

            
################################################################################
class OpenRAVEKinBodyWrapper(object):
    """A wrapper for an OpenRAVE KinBody. When calling SetTransform and GetTransform on the
    original KinBody, it will set and get the transform of the object's local frame, which
    can be at arbitrary position relative to the object. This class acts as another
    interface layer to the object such that SetTransform and GetTransform will set and get
    the transform of the frame attached to the object's COM instead.

    """    
    def __init__(self, mobj, TcomLocal):
        self.mobj = mobj
        self.Tcom = np.array(TcomLocal)
        self.TcomInv = np.linalg.inv(self.Tcom)


    def GetEnv(self):
        return self.mobj.GetEnv()

    
    def GetTransform(self):
        return np.dot(self.mobj.GetTransform(), self.Tcom)

    
    def SetTransform(self, T):
        self.mobj.SetTransform(np.dot(T, self.TcomInv))

    
################################################################################
def VisualizeManipulationTrajectory(manipTraj, lrobot, rrobot, orwrapper, timestep=0.005):
    env = lrobot.GetEnv()
    lcontroller = lrobot.GetController()
    rcontroller = rrobot.GetController()
    # Scene prep.: set object transform
    for (traj, trajtype) in zip(manipTraj.trajsList, manipTraj.trajTypesList):
        if trajtype == manipTraj.TRANSFER:
            orwrapper.SetTransform(traj.se3_traj.Eval(0))
            break

    for (traj, trajtype) in zip(manipTraj.trajsList, manipTraj.trajTypesList):
        if trajtype == manipTraj.TRANSIT:
            if traj.lFirst:
                if traj.lTraj is not None:
                    lcontroller.SetPath(traj.lTraj)
                    lrobot.WaitForController(0)
                if traj.rTraj is not None:
                    rcontroller.SetPath(traj.rTraj)                    
                    rrobot.WaitForController(0)
            else:
                if traj.rTraj is not None:
                    rcontroller.SetPath(traj.rTraj)                    
                    rrobot.WaitForController(0)
                if traj.lTraj is not None:
                    lcontroller.SetPath(traj.lTraj)
                    lrobot.WaitForController(0)
        else: # TRANSFER
            for t, (ql, qr) in zip(traj.timestamps,
                                   zip(traj.bimanual_wpts[0], traj.bimanual_wpts[1])):
                orwrapper.SetTransform(traj.se3_traj.Eval(t))
                lrobot.SetActiveDOFValues(ql)
                rrobot.SetActiveDOFValues(qr)
                time.sleep(traj.timestep)
                    

################################################################################
class RectangularPlacementRegion(object):
    """
    """    
    def __init__(self, dx, dy):
        """
        Parameters
        ----------
        dx : float
            Half width of the region in x-direction
        dy : float
            Half width of the region in y-direction
        """
        self.dx = dx
        self.dy = dy
        self._area = 4*self.dx*self.dy
        

    def Sample(self):
        """Sample a point in the region.

        """
        point = np.array([_rng.uniform(-self.dx, self.dx),
                          _rng.uniform(-self.dy, self.dy)])
        return point


class PlacementRegion(object):
    """A set of RectangularPlacementRegions, together with information of their relative
    positions, orientations, etc.

    """
    def __init__(self, regions=None, positions=None, angles=None):
        """Store a set of RectangularPlacementRegions.

        Parameters
        ----------
        regions : RectangularPlacementRegion, optional
            List of RectangularPlacementRegions
        positions : list, optional
            List of positions of each Region w.r.t. the origin of supporting surface's
            frame
        angles : list, optional
            List of angles of rotation of each Region (around the thrid axis specified 
            by Trest in MyObject) w.r.t. the origin of supporting surface's frame
        """
        if regions is not None:
            assert(positions is not None and angles is not None)
            n = len(regions)
            assert(len(positions) == n)
            assert(len(angles) == n)
            self.regions   = regions
            self.positions = positions
            self.angles    = angles
            self._areas    = [r._area for r in self.regions]
        else:
            self.regions   = []
            self.positions = []
            self.angles    = []
            self._areas    = []


    def AddRegion(self, region, position, angle):
        self.regions.append(region)
        self.positions.append(position)
        self.angles.append(angle)
        self._areas.append(region._area)


    def Sample(self):
        totalArea = sum(self._areas)
        p = _rng.uniform(0, totalArea)
        s = 0
        localPoint = None
        regionIndex = None
        for i, (r, a) in enumerate(zip(self.regions, self._areas)):
            s += a
            if p <= s:
                localPoint = r.Sample()
                regionIndex = i
                break
        assert(regionIndex is not None and localPoint is not None)
        theta = self.angles[regionIndex]
        c = np.cos(theta)
        s = np.sin(theta)
        R = np.array([[c, -s], [s, c]])
        point = np.dot(R, localPoint)
        point += self.positions[regionIndex]
        return point
