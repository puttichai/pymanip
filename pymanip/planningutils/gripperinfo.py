# -*- coding: utf-8 -*-
#
# Copyright (C) 2016 Puttichai Lertkultanon <L.Puttichai@gmail.com>
#
# This file is part of pymanip.
#
# pymanip is free software: you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# pymanip is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# pymanip. If not, see <http://www.gnu.org/licenses/>.
from os.path import join
import pkg_resources

class GripperInfo(object):
    """Basic information of a gripper.

    """
    def __init__(self, L, dmax, gripperOffset, xmlFileName):
        """
        """
        self._L = L
        self._dmax = dmax
        self._gripperOffset = gripperOffset
        self._XMLData = pkg_resources.resource_string('pymanip', join('models', xmlFileName))

    @property
    def L(self):
        """L is an approximated length of the gripper + the link the gripper attached to. This
        gives the idea of how high the to-be-grasped link needs to be in order for the
        robot to be able to grasp it from below

        """
        return self._L

    @property
    def dmax(self):
        """dmax is the maximum width of the object that this gripper can grasp.

        """
        return self._dmax

    @property
    def gripperOffset(self):
        """gripperOffset determines how high the to-be-grasped link should be in order for the
        gripper to approaching the link from sideway.

        """
        return self._gripperOffset


class Robotiq2F85Info(GripperInfo):
    """Basic information of Robotiq 2-Finger 85 Gripper.

    """
    def __init__(self):
        super(Robotiq2F85Info, self).__init__(0.350, 0.0425, 0.08, 'gripper_2f85_aabb.xml')
