# -*- coding: utf-8 -*-
#
# Copyright (C) 2016 Puttichai Lertkultanon <L.Puttichai@gmail.com>
#
# This file is part of pymanip.
#
# pymanip is free software: you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# pymanip is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# pymanip. If not, see <http://www.gnu.org/licenses/>.

"""The planner solves bimanual queries by making use of certificates provided by issuer.

"""

import openravepy as orpy
import numpy as np
import networkx as nx
import logging
import random
from . import miscplanners as mp
from ..planningutils.trajectory import TransitTrajectory
from bimanual.utils.misc_planning import plan_transit_motion

loglevel = logging.DEBUG
logging.basicConfig(format='[%(levelname)s] [%(name)s: %(funcName)s] %(message)s', level=loglevel)
log = logging.getLogger(__name__)
rng = random.SystemRandom()
ikfilter = orpy.IkFilterOptions.CheckEnvCollisions

class BQuery(object):

    def __init__(self, Tstart, Tgoal, Ttype='object'):
        """todo: accept grasps as optional arguments.

        """
        self.Tstart = Tstart
        self.Tgoal = Tgoal
        self.Ttype = Ttype


class BQuerySolver(object):

    # def __init__(self, robots, orwrapper, myObject, placementConnectionsList):
    def __init__(self, robots, orwrapper, myObject, placementConnectionsList, paddedObject):
        self.lrobot, self.rrobot = robots
        self.orwrapper = orwrapper
        self.myObject = myObject
        self.env = self.myObject.mobj.GetEnv()
        self.placementConnections = nx.Graph()
        for e in placementConnectionsList: # storing certificates information
            self.placementConnections.add_edge(*e)
        # self.se2planner = mp.BimanualSE2Planner([self.lrobot, self.rrobot], self.myObject.mobj, self.myObject)
        self.se2planner = mp.BimanualSE2PlannerWithPaddedObject([self.lrobot, self.rrobot], self.myObject.mobj, self.myObject, paddedObject)
        

    def Solve(self, bquery, timeOut=60.0):
        if bquery.Ttype == 'object':
            TcomStart = np.dot(bquery.Tstart, self.myObject.Tcom)
            TcomGoal = np.dot(bquery.Tgoal, self.myObject.Tcom)
        else: # Ttype == 'com'
            TcomStart = bquery.Tstart
            TcomGoal = bquery.Tstart
        qobjStart = self.myObject.ComputeQObject(TcomStart, forRealObject=False)
        qobjGoal = self.myObject.ComputeQObject(TcomGoal, forRealObject=False)

        placementStart = qobjStart[0]
        placementGoal = qobjGoal[0]
        placementSequence = nx.shortest_path(self.placementConnections,
                                             source=placementStart, target=placementGoal)
        if len(placementSequence) == 1:
            # Tstart and Tgoal are in the same placement class
            manipTraj = self.se2planner.Solve(TcomStart, TcomGoal, Ttype='com')
            return manipTraj

        # Fetch data from the certificate
        cctrajs = []
        for i in xrange(len(placementSequence) - 1):
            pair = (placementSequence[i], placementSequence[i + 1])
            _reversed = False
            if pair not in self.placementConnections.edges():
                assert(pair[::-1] in self.placementConnections.edges())
                _reversed = True
                
            data = self.placementConnections.get_edge_data(*pair)
            ccquery = data['certificate']
            if _reversed:
                cctraj = ccquery.cctraj.reverse()
            else:
                cctraj = ccquery.cctraj
            cctrajs.append(cctraj)

        # Plan necessary in-placement transfer trajectories
        manipTrajs = []
        manipTraj = self.se2planner.Solve(TcomStart, cctrajs[0].se3_traj.Eval(0), Ttype='com')
        manipTrajs.append(manipTraj)
        for i in xrange(len(cctrajs) - 1):
            se3traj = cctrajs[i].se3_traj
            nextse3traj = cctrajs[i + 1].se3_traj
            manipTraj = self.se2planner.Solve(se3traj.Eval(se3traj.duration), nextse3traj.Eval(0), Ttype='com')
            manipTrajs.append(manipTraj)
        se3traj = cctrajs[-1].se3_traj
        manipTraj = self.se2planner.Solve(se3traj.Eval(se3traj.duration), TcomGoal, Ttype='com')
        manipTrajs.append(manipTraj)

        # Assemble all trajectories into the final manipulation trajectory
        finalManipTraj = manipTrajs[0]        
        threshold = 1e-7
        for (cctraj, manipTraj) in zip(cctrajs, manipTrajs[1:]):
            # Bridge the gap (in robot configurations) between prev manipTraj and cctraj
            T = cctraj.se3_traj.Eval(0)
            qlstart = finalManipTraj.trajsList[-1].bimanual_wpts[0][-1]
            qlgoal = cctraj.bimanual_wpts[0][0]
            qrstart = finalManipTraj.trajsList[-1].bimanual_wpts[1][-1]
            qrgoal = cctraj.bimanual_wpts[1][0]
            with self.env:
                self.orwrapper.SetTransform(T)
            qldiff = qlstart - qlgoal
            if np.sqrt(np.dot(qldiff, qldiff)) > threshold:
                with self.env:
                    self.rrobot.SetActiveDOFValues(qrstart)
                lTraj = plan_transit_motion(self.lrobot, qlstart, qlgoal,
                                            pregrasp_start=True, pregrasp_goal=True)
            else:
                lTraj = None
            qrdiff = qrstart - qrgoal
            if np.sqrt(np.dot(qldiff, qldiff)) > threshold:
                with self.env:
                    self.lrobot.SetActiveDOFValues(qlgoal)
                rTraj = plan_transit_motion(self.rrobot, qrstart, qrgoal,
                                            pregrasp_start=True, pregrasp_goal=True)
            else:
                rTraj = None
            transitTraj = TransitTrajectory(lTraj, rTraj)
            finalManipTraj.AddTrajectory(transitTraj, finalManipTraj.TRANSIT)
            
            finalManipTraj.AddTrajectory(cctraj, finalManipTraj.TRANSFER)

            # Bridge the gap (in robot configurations) between cctraj and next manipTraj
            T = cctraj.se3_traj.Eval(cctraj.se3_traj.duration)
            qlstart = cctraj.bimanual_wpts[0][-1]
            qlgoal = manipTraj.trajsList[0].bimanual_wpts[0][0]
            qrstart = cctraj.bimanual_wpts[1][-1]
            qrgoal = manipTraj.trajsList[0].bimanual_wpts[1][0]
            with self.env:
                self.orwrapper.SetTransform(T)
            qldiff = qlstart - qlgoal
            if np.sqrt(np.dot(qldiff, qldiff)) > threshold:
                with self.env:
                    self.rrobot.SetActiveDOFValues(qrstart)
                lTraj = plan_transit_motion(self.lrobot, qlstart, qlgoal,
                                            pregrasp_start=True, pregrasp_goal=True)
            else:
                lTraj = None
            qrdiff = qrstart - qrgoal
            if np.sqrt(np.dot(qldiff, qldiff)) > threshold:
                with self.env:
                    self.lrobot.SetActiveDOFValues(qlgoal)
                rTraj = plan_transit_motion(self.rrobot, qrstart, qrgoal,
                                            pregrasp_start=True, pregrasp_goal=True)
            else:
                rTraj = None
            transitTraj = TransitTrajectory(lTraj, rTraj)
            finalManipTraj.AddTrajectory(transitTraj, finalManipTraj.TRANSIT)
            
            finalManipTraj.AddManipTrajectory(manipTraj)
            
        return finalManipTraj

