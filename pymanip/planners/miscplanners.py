import openravepy as orpy
import numpy as np
import logging
import random
import time
from itertools import product
from TOPP import Trajectory
from toppso3 import Utils as se3utils
from toppso3 import Heap as heap

from ..planningutils.utils import Colorize, OpenRAVEKinBodyWrapper
from ..planningutils import utils
from ..planningutils.trajectory import TransitTrajectory, ManipulationTrajectory
from bimanual.utils.trajectory import SE3Trajectory, CCTrajectory
from bimanual.utils import lie
from bimanual.utils import misc_planning as mp

loglevel = logging.DEBUG
logging.basicConfig(format='[%(levelname)s] [%(name)s: %(funcName)s] %(message)s', level=loglevel)
log = logging.getLogger(__name__)
rng = random.SystemRandom()
FW = 0
BW = 1
REACHED  = 0
ADVANCED = 1
TRAPPED  = 2
ikfilter = orpy.IkFilterOptions.CheckEnvCollisions
epsilon = 1e-8

################################################################################
# SE(2) Planner (modified from TOPP-SO3's SE(3) planner)
class Config(object):
    """An SE(3) configuration.

    """
    def __init__(self, q, qt, qs=None, qts=None, qss=None, qtss=None, qobject=None):
        """Initialize an SE(3) configuration.

        Parameters
        ----------
        q : numpy.ndarray (length 4)
            Quaternion
        qt : numpy.ndarray (length 3)
            Translation
        qs : numpy.ndarray (length 3)
            Angular velocity
        qts : numpy.ndarray (length 3)
            Linear velocity
        qss : numpy.ndarray (length 3)
            Angular acceleration
        qtss : numpy.ndarray (length 3)
            Linear acceleration
        qobject : list
            Placement parameters (see planningutils.myobject for more info)
        
        """
        nq = np.linalg.norm(q)
        assert(nq > 0)
        self.q = q / nq
        self.qs = np.array([1e-3, 1e-3, 1e-3]) if qs is None else qs

        self.qt = qt
        self.qts = np.zeros(3) if qts is None else qts
        self.qobject = qobject
        self.T = orpy.matrixFromPose(np.hstack([self.q, self.qt]))

    @staticmethod
    def FromMatrix(T, qobject=None):
        """Initialize an SE(3) configuration from a 4x4 transformation matrix.
        
        Parameters
        ----------
        T : numpy.adarray
            Homogeneous transformation
        qobject : list
            Placement parameters (see planningutils.myobject for more info)
        
        """
        q = orpy.quatFromRotationMatrix(np.copy(T[0:3, 0:3]))
        qt = np.copy(T[0:3, 3])
        return Config(q, qt, qobject=qobject)


class Vertex(object):
    """A vertex contained in a planning tree
    
    """
    def __init__(self, config, vertextype=FW):
        """Initialize a vertex.

        Parameters
        ----------
        config : Config
        vertextype : FW, BW
        
        """
        self.config = config
        self.vertextype = vertextype
        # The following members are to be assigned when the vertex is added to a tree
        self.index = 0
        self.parentindex = None
        self.rotationTraj = None
        self.translationTraj = None
        self.level = 0


class Tree(object):
    """A tree for a motion planner.

    """
    def __init__(self, vroot, treetype=FW):
        """Initialize a tree.

        Parameters
        ----------
        vroot : Vertex
        treetype : FW, BW
        
        """
        self.vertices = [vroot]
        self.treetype = treetype

        
    def __len__(self):
        return len(self.vertices)

    
    def __getitem__(self, index):
        return self.vertices[index]        
    
    
    def AddVertex(self, vnew, parentindex, rTraj, tTraj):
        """Add a given vertex to the tree.
        
        Parameters
        ----------
        vnew : Vertex
            The given vertex to be added to this tree
        parentindex : int
            The index of the parent of this vertex
        rTraj : TOPP.Trajectory
            Rotation trajectory
        tTraj : TOPP.Trajectory
            Translation trajectory
        
        """
        vnew.parentindex = parentindex
        vnew.rotationTraj = rTraj
        vnew.translationTraj = tTraj
        vnew.level = self[parentindex].level + 1
        vnew.index = len(self)
        self.vertices.append(vnew)


    def GenerateRotationTrajList(self, index=-1):
        """Obtain a list of rotation trajectories tracing from the vertex of the specified index
        to the root of the tree.

        Parameters
        ----------
        index : int
            The index of the vertex to start tracing back

        """
        rotationTrajList = []

        vertex = self.vertices[index]
        while vertex.parentindex is not None:
            parent = self.vertices[vertex.parentindex]
            rotationTrajList.append(vertex.rotationTraj)
            vertex = parent
        if self.treetype == FW:
            rotationTrajList.reverse()
        return rotationTrajList


    def GenerateRotationMatricesList(self, index=-1):
        """Obtain a list of rotation matrices of vertices along the path tracing from the vertex
        of the specified index to the root of the tree.

        Parameters
        ----------
        index : int
            The index of the vertex to start tracing back

        """
        rotationMatricesList = []
        
        vertex = self.vertices[index]
        while vertex.parentindex is not None:
            parent = self.vertices[vertex.parentindex]
            rotationMatricesList.append(vertex.config.T[0:3, 0:3])
            vertex = parent
        rotationMatricesList.append(self.vertices[0].config.T[0:3, 0:3])
        if self.treetype == FW:
            rotationMatricesList.reverse()
        return rotationMatricesList
    
        
    def GenerateTranslationTrajList(self, index=-1):
        """Obtain a list of translation trajectories tracing from the vertex of the specified
        index to the root of the tree.

        Parameters
        ----------
        index : int
            The index of the vertex to start tracing back

        """
        transTrajList = []

        vertex = self.vertices[index]
        while vertex.parentindex is not None:
            parent = self.vertices[vertex.parentindex]
            transTrajList.append(vertex.translationTraj)
            vertex = parent
        if self.treetype == FW:
            transTrajList.reverse()
        return transTrajList


class SE2Query(object):
    def __init__(self, mobj, TcomStart, TcomGoal, myObject, **kwargs):
        """A query for SE(2) motion planning. The given transformations, Tstart and Tgoal, are
        transformations for the movable object. However, in planning, all transformations
        stored in Configs are that of the OpenRAVEKinBodyWrapper. (qobject stored in a
        config is still that of the real object, though.)

        Parameters
        ----------
        mobj : openravepy.KinBody
        Tstart : numpy.ndarray
        Tgoal : numpy.ndarray
        myObject : myobject
        nn : int (optional)
            The number of nearest neighbors to be considered when extending a tree. 
            Default = -1, meaning that all vertices in the tree will be considered.
        stepSize : float (optional)
            Step size for tree extension step. Default = 0.7
        interpolationDuration : float (optional)
            Duration for trajectory interpolation. Default = twice stepSize
        discrTimeStep : float (optional)
            Time step for collision checking along a trajectory. Default = 0.005

        """
        passed, qobjStart, qobjGoal = self._CheckQuery(TcomStart, TcomGoal, myObject)
        self.myObject = myObject
        cStart = Config.FromMatrix(TcomStart, qobjStart)
        cGoal = Config.FromMatrix(TcomGoal, qobjGoal)
        self.placementIndex = qobjStart[0]
        self.vStart = Vertex(cStart, FW)
        self.vGoal = Vertex(cGoal, BW)
        self.treeStart = Tree(self.vStart, FW)
        self.treeEnd = Tree(self.vGoal, BW)
        self._InitializeParameters(**kwargs)

        self.mobj = OpenRAVEKinBodyWrapper(mobj, myObject.Tcom)
        self._ComputeTranslationLimits(qobjStart, qobjGoal)


    def _CheckQuery(self, TcomStart, TcomGoal, myObject):
        """Check if Tstart and Tgoal are in the same placement class.

        Parameters
        ----------
        Tstart : numpy.ndarray
        Tgoal : numpy.ndarray
        myObject : myobject

        Returns
        -------
        isSimilar : bool
            True if the two transformations are in the same placement class.
        qobjStart : list
            Placement parameters for Tstart
        qobjGoal : list
            Placement parameters for Tgoal

        """
        qobjStart = myObject.ComputeQObject(TcomStart, forRealObject=False)
        qobjGoal = myObject.ComputeQObject(TcomGoal, forRealObject=False)
        return qobjStart[0] == qobjGoal[0], qobjStart, qobjGoal


    def _ComputeTranslationLimits(self, qobjStart, qobjGoal):
        xMin, xMax = sorted([qobjStart[1], qobjGoal[1]])
        yMin, yMax = sorted([qobjStart[2], qobjGoal[2]])
        self.lowerLimits = [xMin - 0.1,
                            yMin - 0.1]
        self.upperLimits = [xMax + 0.1,
                            yMax + 0.1]
        # New data structure
        self.placementRegion = utils.PlacementRegion()
        rect1 = utils.RectangularPlacementRegion(0.325, 0.10)
        rect2 = utils.RectangularPlacementRegion(0.10, 0.70)
        self.placementRegion.AddRegion(rect1, np.array([0.075, 0]), 0)
        self.placementRegion.AddRegion(rect2, np.array([0.5, 0]), 0)


    def SampleConfig(self):
        x, y = self.placementRegion.Sample()
        theta = rng.uniform(-np.pi, np.pi)
        qobject = [self.placementIndex, x, y, theta]
        Tcom = self.myObject.ComputeTObject(qobject, forRealObject=False)
        config = Config.FromMatrix(Tcom, qobject=qobject)
        return config


    def _InitializeParameters(self, **kwargs):
        """Initialize parameters to be used in planning.

        """
        # self.nn = kwargs.get('nn', -1)
        self.nn = kwargs.get('nn', 100)
        self.stepSize = kwargs.get('stepSize', 0.25)
        self.interpolationDuration = kwargs.get('interpolationDuration', 2.0*self.stepSize)
        self.discrTimeStep = kwargs.get('discrTimeStep', 0.005)
        
        self.connectingTranslationTraj = None
        self.connectingRotationTraj = None
        self._runningTime = 0.0
        self._iterations = 0
        self._solved = False
        

    def GenerateFinalLieTraj(self):
        """
        """
        if not self._solved:
            log.info("Query not solved yet.")
            return None

        rotationTrajList = []
        rotationTrajList += self.treeStart.GenerateRotationTrajList()
        rotationTrajList.append(self.connectingRotationTraj)
        rotationTrajList += self.treeEnd.GenerateRotationTrajList()

        rotationMatricesList = []
        rotationMatricesList += self.treeStart.GenerateRotationMatricesList()
        rotationMatricesList += self.treeEnd.GenerateRotationMatricesList()

        lieTraj = lie.LieTraj(rotationMatricesList, rotationTrajList)
        return lieTraj
        

    def GenerateFinalTranslationTraj(self):
        """
        """
        if not self._solved:
            log.info("Query not solved yet.")
            return None

        translationTrajList = []
        translationTrajList += self.treeStart.GenerateTranslationTrajList()
        translationTrajList.append(self.connectingTranslationTraj)
        translationTrajList += self.treeEnd.GenerateTranslationTrajList()

        chunksList = list(translationTrajList[0].chunkslist)
        for traj in translationTrajList[1:]:
            chunksList.extend(traj.chunkslist)
        return Trajectory.PiecewisePolynomialTrajectory(chunksList)


    def GenerateFinalSE3Traj(self):
        """
        """
        if not self._solved:
            log.info("Query not solved yet.")
            return None
        
        lieTraj = self.GenerateFinalLieTraj()
        transTraj = self.GenerateFinalTranslationTraj()
        se3Traj = SE3Trajectory(lieTraj, transTraj)
        return se3Traj


class RigidBodyRRTPlanner(object):
    
    def __init__(self, query=None):
        self.query = query
        if self.query is not None:
            self._InitializeFromQuery()


    def SetQuery(self, query):
        self.query = query
        self._InitializeFromQuery()


    def _InitializeFromQuery(self):
        self.mobj = self.query.mobj # this is OpenRAVEKinBodyWrapper
        self.env = self.mobj.GetEnv()
        self.SampleConfig = self.query.SampleConfig # the config sampling function


    def Plan(self, timeOut=60):
        if self.query._solved:
            log.info("Query already solved.")
            return True

        t = 0.0
        prevIter = self.query._iterations
        tbeg = time.time()
        # Greedy approach: try to connect the two tree at the beginning
        if self.Connect() == REACHED:
            tend = time.time()
            self.query._runningTime += (tend - tbeg)
            log.info(Colorize('Path found. Totatl iterations: {0}; Total running time: {1} s.'.
                              format(self.query._iterations, self.query._runningTime), 'green'))
            self.query._solved = True
            return True

        t += (time.time() - tbeg)
        while t < timeOut:
            tbeg = time.time()
            self.query._iterations += 1
            log.debug(Colorize('Iteration {0}'.format(self.query._iterations), 'yellow'))

            config = self.SampleConfig()
            if not (self.Extend(config) == TRAPPED):
                log.debug(Colorize('  Tree start: {0}; Tree end: {1}'.\
                format(len(self.query.treeStart), len(self.query.treeEnd)), 'green'))

                if self.Connect() == REACHED:
                    t += (time.time() - tbeg)
                    self.query._runningTime += t
                    log.info(Colorize('Path found. Totatl iterations: {0}; Total running time: {1} s.'.
                    format(self.query._iterations, self.query._runningTime), 'green'))
                    self.query._solved = True
                    return True
                
            t += (time.time() - tbeg)

        self.query._runningtime += t
        log.info(Colorize('Timeout ({0} s.) after {1} iterations'.
                          format(timeOut, self.query._iterations - prevIter), 'red'))
        return False


    def SetSampleFunction(self, f):
        """
        """
        self.SampleConfig = f


    def Extend(self, config):
        """
        """
        if np.mod(self.query._iterations - 1, 2) == FW:
            return self._ExtendFW(config)
        else:
            return self._ExtendBW(config)


    def _ExtendFW(self, config):
        """
        """
        ret = TRAPPED
        nnIndices = self.NearestNeighborIndices(config, FW)
        for nnIndex in nnIndices:
            vNear = self.query.treeStart[nnIndex]
            qbeg = vNear.config.q
            qsbeg = vNear.config.qs
            qtbeg = vNear.config.qt
            qtsbeg = vNear.config.qts

            # Threshold the distance between the neighbor and the config
            dist = self.Distance(config, vNear.config)
            if dist <= self.query.stepSize:
                qend = config.q
                qtend = config.qt
                ret = REACHED
            else:
                qend = qbeg + self.query.stepSize*(config.q - qbeg)/np.sqrt(dist)
                qend /= np.linalg.norm(qend) # normalize the quaternion
                qtend = qtbeg + self.query.stepSize*(config.qt - qtbeg)/np.sqrt(dist)
                ret = ADVANCED
            qsend = config.qs            
            qtsend = config.qts
            # Generate a new configuration
            newConfig = Config(qend, qtend, qsend, qtsend)

            if not self.IsFeasibleConfig(newConfig):
                log.debug("New config not feasible")
                ret = TRAPPED
                continue
            rotationTraj, transTraj = self.InterpolateConfigs(vNear.config, newConfig)
            if not self.IsFeasibleTrajectory(rotationTraj, transTraj, vNear.config.q):
                log.debug("Interpolated trajectory not feasible")
                ret = TRAPPED
                continue
            # Add a new vertex to the tree
            newVertex = Vertex(newConfig, FW)
            self.query.treeStart.AddVertex(newVertex, vNear.index, rotationTraj, transTraj)
            return ret
        return ret


    def _ExtendBW(self, config):
        """
        """
        ret = TRAPPED
        nnIndices = self.NearestNeighborIndices(config, BW)
        for nnIndex in nnIndices:
            vNear = self.query.treeEnd[nnIndex]
            qend = vNear.config.q
            qsend = vNear.config.qs
            qtend = vNear.config.qt
            qtsend = vNear.config.qts

            # Threshold the distance between the neighbor and the config
            dist = self.Distance(vNear.config, config)
            if dist <= self.query.stepSize:
                qbeg = config.q
                qtbeg = config.qt
                ret = REACHED
            else:
                qbeg = qend + self.query.stepSize*(config.q - qend)/np.sqrt(dist)
                qbeg /= np.linalg.norm(qbeg) # normalize the quaternion
                qtbeg = qtend + self.query.stepSize*(config.qt - qtend)/np.sqrt(dist)
                ret = ADVANCED
            qsbeg = config.qs
            qtsbeg = config.qts
            # Generate a new configuration
            newConfig = Config(qbeg, qtbeg, qsbeg, qtsbeg)

            if not self.IsFeasibleConfig(newConfig):
                log.debug("New config not feasible")
                ret = TRAPPED
                continue
            rotationTraj, transTraj = self.InterpolateConfigs(newConfig, vNear.config)
            if not self.IsFeasibleTrajectory(rotationTraj, transTraj, newConfig.q):
                log.debug("Interpolated trajectory not feasible")
                ret = TRAPPED
                continue
            # Add a new vertex to the tree
            newVertex = Vertex(newConfig, BW)
            self.query.treeEnd.AddVertex(newVertex, vNear.index, rotationTraj, transTraj)
            return ret
        return ret
            

    def Connect(self):
        """
        """
        if np.mod(self.query._iterations - 1, 2) == FW:
            return self._ConnectFW()
        else:
            return self._ConnectBW()

    
    def _ConnectFW(self):
        """
        """
        vTest = self.query.treeStart[-1] # the most newly added vertex
        nnIndices = self.NearestNeighborIndices(vTest.config, BW)
        for nnIndex in nnIndices:
            vNear = self.query.treeEnd[nnIndex]
            rotationTraj, transTraj = self.InterpolateConfigs(vTest.config, vNear.config)
            if self.IsFeasibleTrajectory(rotationTraj, transTraj, vTest.config.q):
                log.info("Successful connection")
                self.query.treeEnd.vertices.append(vNear)
                self.query.connectingTranslationTraj = transTraj
                self.query.connectingRotationTraj = rotationTraj
                return REACHED
        return TRAPPED


    def _ConnectBW(self):
        """
        """
        vTest = self.query.treeEnd[-1] # the most newly added vertex
        nnIndices = self.NearestNeighborIndices(vTest.config, FW)
        for nnIndex in nnIndices:
            vNear = self.query.treeStart[nnIndex]
            rotationTraj, transTraj = self.InterpolateConfigs(vNear.config, vTest.config)
            if self.IsFeasibleTrajectory(rotationTraj, transTraj, vNear.config.q):
                log.debug("Successful connection")
                self.query.treeStart.vertices.append(vNear)
                self.query.connectingTranslationTraj = transTraj
                self.query.connectingRotationTraj = rotationTraj
                return REACHED
        return TRAPPED


    def Distance(self, c1, c2):
        """Compute the distance between the given two configurations.

        """
        return se3utils.SE3Distance(c1.T, c2.T, 1/np.pi, 1.0)


    def InterpolateConfigs(self, c1, c2):
        """Generate a rotation trajectory and a translation trajectory (both in TOPP format) which
        interpolate the two given configurations.

        """
        rotationTraj = lie.InterpolateSO3(orpy.rotationMatrixFromQuat(c1.q),
                                          orpy.rotationMatrixFromQuat(c2.q),
                                          c1.qs, c2.qs, self.query.interpolationDuration)
        transTrajStr = se3utils.TrajString3rdDegree(c1.qt, c2.qt, c1.qts, c2.qts,
                                                    self.query.interpolationDuration)
        transTraj = Trajectory.PiecewisePolynomialTrajectory.FromString(transTrajStr)
        return rotationTraj, transTraj


    def IsFeasibleConfig(self, config):
        """
        """
        with self.mobj.mobj:
            self.mobj.SetTransform(config.T)
            passed = not self.env.CheckCollision(self.mobj.mobj)
        return passed            


    def IsFeasibleTrajectory(self, rotationTraj, transTraj, initQuat):
        """
        """
        # Check translational velocity limits
        tVelLimit = 0.2 # should not move in any direction faster than 20 cm/s
        for chunk in transTraj.chunkslist:
            for p in chunk.polynomialsvector:
                # p is a 3rd degree polynomial (TOPP Trajectory.Polynomial). Therefore,
                # the velocity polynomial is parabolic. We need to check for extremum at
                # three points: t = 0, t = duration, and t = critical value if the
                # critical value falls in the valid range.
                d, c, b, a = p.coeff_list
                tCritical = -b/(3*a) # a is always non-zero
                vExtreme = max(abs(p.Evald(0)), abs(p.Evald(chunk.duration)))
                if vExtreme > tVelLimit:
                    return False
                if tCritical > chunk.duration or tCritical < 0:
                    continue
                else:
                    if abs(p.Evald(tCritical)) > tVelLimit:
                        return False

        # Check rotational velocity limits
        rVelLimit = 1.0
        for chunk in rotationTraj.chunkslist:
            for p in chunk.polynomialsvector:
                # p is a 3rd degree polynomial (TOPP Trajectory.Polynomial). Therefore,
                # the velocity polynomial is parabolic. We need to check for extremum at
                # three points: t = 0, t = duration, and t = critical value if the
                # critical value falls in the valid range.
                d, c, b, a = p.coeff_list
                tCritical = -b/(3*a) # a is always non-zero
                vExtreme = max(abs(p.Evald(0)), abs(p.Evald(chunk.duration)))
                if vExtreme > rVelLimit:
                    return False
                if tCritical > chunk.duration or tCritical < 0:
                    continue
                else:
                    if abs(p.Evald(tCritical)) > rVelLimit:
                        return False
        
        # Check collision
        M = np.eye(4)
        Rbeg = orpy.rotationMatrixFromQuat(initQuat)
        duration = rotationTraj.duration
        T = np.append(np.arange(0, duration, self.query.discrTimeStep), duration)
        passed = True
        with self.mobj.mobj:
            for t in T:
                M[0:3, 0:3] = lie.EvalRotation(Rbeg, rotationTraj, t)
                M[0:3, 3] = transTraj.Eval(t)
                self.mobj.SetTransform(M)
                passed = not self.env.CheckCollision(self.mobj.mobj)
                if not passed:
                    break
        return passed            


    def NearestNeighborIndices(self, c, treeType):
        tree = self.query.treeStart if treeType == FW else self.query.treeEnd
        nv = len(tree)

        distanceList = [self.Distance(c, v.config) for v in tree]
        distanceHeap = heap.Heap(distanceList)

        if self.query.nn == -1:
            # Consider all vertices in the tree as neighbors
            nn = nv
        else:
            nn = min(self.query.nn, nv)
        nnIndices = [distanceHeap.ExtractMin()[0] for i in xrange(nn)]
        return nnIndices


    def Shortcut(self, se3Traj, maxIter=100):
        finalSE3Traj = se3Traj.duplicate()
        if finalSE3Traj.duration <= self.query.interpolationDuration:
            log.info("The current se3Traj duration is {0} s. (interpolation duration = {1}) "
                     "No shortcut needed.".\
                     format(finalSE3Traj.duration, self.query.interpolationDuration))
            return finalSE3Traj

        nsuccess = 0
        for i in xrange(maxIter):
            dur = finalSE3Traj.duration
            # Make sure the two time instants are separated by at least the interpolation duration.
            t0 = rng.uniform(0, dur - self.query.interpolationDuration)
            t1 = rng.uniform(t0 + self.query.interpolationDuration, dur)

            # Evaluate boundary conditions
            T0 = finalSE3Traj.Eval(t0)
            T1 = finalSE3Traj.Eval(t1)
            config0 = Config.FromMatrix(T0)
            config1 = Config.FromMatrix(T1)
            rotationTraj, transTraj = self.InterpolateConfigs(config0, config1)
            if not self.IsFeasibleTrajectory(rotationTraj, transTraj, config0.q):
                log.debug("Iteration {0}: trajectory not feasible".format(i + 1))
                continue
            lieTraj = lie.LieTraj([utils.ExtractRotationMatrix(T0)], [rotationTraj])
            newSE3Traj = SE3Trajectory(lieTraj, transTraj)

            remSE3Traj1 = finalSE3Traj.cut(t0)
            remSE3Traj2 = remSE3Traj1.cut(t1 - t0)
            finalSE3Traj.append_traj(newSE3Traj)
            finalSE3Traj.append_traj(remSE3Traj2)
            nsuccess += 1
        log.info("Finished shortcutting: successful {0}/{1} times".format(nsuccess, maxIter))
        return finalSE3Traj
            

class BimanualSE2MotionTracker(object):
    """A planner which computes waypoints for the given two robots to track the object
    trajectory (in SE(2)). Since the object motion is in SE(2), the object is always on a
    supporting surface. Therefore, the robots can change its grasp any time.

    """
    def __init__(self, robots, mobj, myObject, **kwargs):
        self.lrobot, self.rrobot = robots
        self.lmanip = self.lrobot.GetActiveManipulator()
        self.rmanip = self.rrobot.GetActiveManipulator()
        self.mobj = mobj
        self.myObject = myObject
        self.env = self.lrobot.GetEnv()

        self.discrTimeStep = kwargs.get('discrTimeStep', 0.005)
        self.maxIter = kwargs.get('maxIter', 8)
        self.tol = kwargs.get('tol', 1e-7)
        self.threshangle = kwargs.get('threhangle', np.pi * 0.45)

        self.qmin, self.qmax = self.lrobot.GetActiveDOFLimits()
        self.vmax = self.lrobot.GetDOFVelocityLimits()[0:6]
        self.qdmax = self.vmax * self.discrTimeStep


    def Track(self, se3Traj):
        # import IPython; IPython.embed(header="track")
        Tobj = se3Traj.Eval(0)
        placementIndex = self.myObject.ComputeQObject(Tobj, forRealObject=False)[0]
        self._FindGraspCandidates(placementIndex) # obtain all possible approaching directions
        # Generate a list of time instants to find IK solutions
        timestamps = np.arange(self.discrTimeStep, se3Traj.duration, self.discrTimeStep)
        if se3Traj.duration - timestamps[-1] > 1e-6:
            timestamps = np.append(timestamps, se3Traj.duration)

        # Parameters
        ntrials = 20
        approachingDirections = self.approachingdirs[:] # computed from _FindGraspCandidates
        approachingPairs = [pair for pair in product(approachingDirections, approachingDirections)]
        reached = False # indicates if the final time instant is reached
        steps = timestamps[:] # make a copy of timestamps
        se3Traj_ = se3Traj.duplicate()

        l_needsQGrasp = True
        r_needsQGrasp = True
        lCurSol = None
        rCurSol = None
        curTobj = Tobj
        se3TrajStartTime = 0

        ccTrajs = []

        rprevappdir = None
        lprevappdir = None
        while not reached:
            # prevappdir = [] # for the case when only one robot need regrasping
            passed = False
            rng.shuffle(approachingPairs)
            for (lappdir, rappdir) in approachingPairs:
                if l_needsQGrasp and lappdir == lprevappdir:
                    continue
                if r_needsQGrasp and rappdir == rprevappdir:
                    continue
                lGraspedLinkIndex = int(lappdir)/6
                rGraspedLinkIndex = int(rappdir)/6
                if lGraspedLinkIndex == rGraspedLinkIndex:
                    # Both hands shouldn't grasp the same link
                    continue
                if l_needsQGrasp and rprevappdir is not None:
                    # Left gripper changes grasp, check if it grasps the same link as the right gripper
                    if int(rprevappdir)/6 == lGraspedLinkIndex:
                        continue
                if r_needsQGrasp and lprevappdir is not None:
                    # Right gripper changes grasp, check if it grasps the same link as the left gripper
                    if int(lprevappdir)/6 == rGraspedLinkIndex:
                        continue
                for itrial in xrange(ntrials):
                    # Sample qgrasp(s) and corresponding IK(s)
                    self.mobj.SetTransform(curTobj)
                    if l_needsQGrasp and r_needsQGrasp:
                        self.rrobot.SetActiveDOFValues(np.zeros(6)) # home
                        lqgrasp = self.myObject.SampleQGrasp(placementIndex, lappdir)
                        # lTgripper = utils.ComputeTGripper2(self.mobj.mobj, lqgrasp[0], lqgrasp)
                        lTgripper = self.myObject.ComputeTGripper(lqgrasp)
                        lCurSol = self.lmanip.FindIKSolution(lTgripper, ikfilter)
                        if lCurSol is None:
                            continue

                        rqgrasp = self.myObject.SampleQGrasp(placementIndex, rappdir)
                        # rTgripper = utils.ComputeTGripper2(self.mobj.mobj, rqgrasp[0], rqgrasp)
                        rTgripper = self.myObject.ComputeTGripper(rqgrasp)
                        self.lrobot.SetActiveDOFValues(lCurSol)
                        rCurSol = self.rmanip.FindIKSolution(rTgripper, ikfilter)
                        if rCurSol is None:
                            continue
                    elif l_needsQGrasp:
                        self.rrobot.SetActiveDOFValues(rCurSol) # home
                        lqgrasp = self.myObject.SampleQGrasp(placementIndex, lappdir)
                        # lTgripper = utils.ComputeTGripper2(self.mobj.mobj, lqgrasp[0], lqgrasp)
                        lTgripper = self.myObject.ComputeTGripper(lqgrasp)
                        lCurSol = self.lmanip.FindIKSolution(lTgripper, ikfilter)
                        if lCurSol is None:
                            continue
                    else: # r_needsQGrasp
                        self.lrobot.SetActiveDOFValues(lCurSol) # home
                        rqgrasp = self.myObject.SampleQGrasp(placementIndex, rappdir)
                        # rTgripper = utils.ComputeTGripper2(self.mobj.mobj, rqgrasp[0], rqgrasp)
                        rTgripper = self.myObject.ComputeTGripper(rqgrasp)
                        rCurSol = self.rmanip.FindIKSolution(rTgripper, ikfilter)
                        if rCurSol is None:
                            continue
                    # Sampling succeeded
                    reached, [l_needsQGrasp_, r_needsQGrasp_], bWaypoints, timesteps = \
                    self._Track(se3Traj, lCurSol, rCurSol, steps)

                    if len(timesteps) > 3 or reached:
                        l_needsQGrasp = l_needsQGrasp_
                        r_needsQGrasp = r_needsQGrasp_
                        lprevappdir = lappdir
                        rprevappdir = rappdir
                        passed = True
                        lCurSol = bWaypoints[0][-1]
                        rCurSol = bWaypoints[1][-1]
                        steps = steps[len(timesteps) - 1:]

                        if not reached:
                            # If not reaching the end yet, we need to cut the SE3 trajectory
                            remSE3Traj = se3Traj_.cut(timesteps[-1] - se3TrajStartTime)
                            se3TrajStartTime = timesteps[-1]

                            offset = timesteps[0]
                            timesteps = [t - offset for t in timesteps]
                            ccTraj = CCTrajectory(se3Traj_, bWaypoints, timesteps)
                            se3Traj_ = remSE3Traj
                            curTobj = se3Traj_.Eval(0)
                        else:
                            offset = timesteps[0]
                            timesteps = [t - offset for t in timesteps]
                            ccTraj = CCTrajectory(se3Traj_, bWaypoints, timesteps)
                        ccTrajs.append(ccTraj)
                        break
                    
                if passed:
                    break
            if not passed:
                import IPython; IPython.embed(header="tracker: no solution!")
                raise Exception
            if len(steps) == 1:
                reached = True
        return ccTrajs                


    def _Track(self, se3Traj, lRefSol, rRefSol, timesteps):
        """Given two robot configurations, using them as reference solutions and track the object
        trajectory (SE(2) trajectory) as far as possible.

        """
        bWaypoints = [[lRefSol], [rRefSol]]
        steps = [timesteps[0]]
        reached = False
        l_needsQGrasp = False
        r_needsQGrasp = False

        curTobj = se3Traj.Eval(timesteps[0])
        with self.lrobot:
            self.lrobot.SetActiveDOFValues(lRefSol)
            lTgripper = self.lmanip.GetTransform()
        with self.rrobot:
            self.rrobot.SetActiveDOFValues(rRefSol)
            rTgripper = self.rmanip.GetTransform()
        lTrel = np.dot(np.linalg.inv(curTobj), lTgripper)
        rTrel = np.dot(np.linalg.inv(curTobj), rTgripper)

        curLSol = bWaypoints[0][-1]
        curRSol = bWaypoints[1][-1]
        for t in timesteps[1:]:
            nextTobj = se3Traj.Eval(t)
            nextLTgripper = np.dot(nextTobj, lTrel)
            nextLSol = self._ComputeIK(nextLTgripper, curLSol, 0)
            if nextLSol is None:
                l_needsQGrasp = True
                
            nextRTgripper = np.dot(nextTobj, rTrel)
            nextRSol = self._ComputeIK(nextRTgripper, curRSol, 1)
            if nextRSol is None:
                r_needsQGrasp = True

            if l_needsQGrasp or r_needsQGrasp:
                return reached, [l_needsQGrasp, r_needsQGrasp], bWaypoints, steps

            # Velocity limits checking
            for j in xrange(6):
                if (abs(nextLSol[j] - curLSol[j]) > self.qdmax[j]):
                    l_needsQGrasp = True
                if (abs(nextRSol[j] - curRSol[j]) > self.qdmax[j]):
                    r_needsQGrasp = True
                if l_needsQGrasp or r_needsQGrasp:
                    print "\t vel limit exceeded"
                    return reached, [l_needsQGrasp, r_needsQGrasp], bWaypoints, steps
            
            # Collision checking
            with self.mobj.mobj:
                self.mobj.SetTransform(nextTobj)
                with self.lrobot:
                    self.lrobot.SetActiveDOFValues(nextLSol)
                    with self.rrobot:
                        self.rrobot.SetActiveDOFValues(nextRSol)
                        if (self.env.CheckCollision(self.lrobot) or self.lrobot.CheckSelfCollision()):
                            l_needsQGrasp = True
                        if (self.env.CheckCollision(self.rrobot) or self.rrobot.CheckSelfCollision()):
                            r_needsQGrasp = True
                        if l_needsQGrasp or r_needsQGrasp:
                            return reached, [l_needsQGrasp, r_needsQGrasp], bWaypoints, steps
            bWaypoints[0].append(nextLSol)
            bWaypoints[1].append(nextRSol)
            steps.append(t)
            print "Added t = {0}".format(t)

            curLSol = bWaypoints[0][-1]
            curRSol = bWaypoints[1][-1]
        reached = True
        return reached, [l_needsQGrasp, r_needsQGrasp], bWaypoints, steps
            
            
    def _ComputeIK(self, T, refSol, robotindex):
        """Compute an IK solution using a differential IK algorithm (driving the robot from the
        given configuration to the desired manipulator transformation using its Jacobian).

        """
        targetPose = orpy.poseFromMatrix(T)
        if targetPose[0] < 0:
            targetPose[0:4] *= -1.0

        reached = False # whether or not the diff IK solver has driven the system to targetPose
        q = np.array(refSol)
        for i in xrange(self.maxIter):
            dq = self._ComputeDQ(targetPose, q, robotindex)
            q += dq
            # Threshold the joint values
            q = np.maximum(np.minimum(q, self.qmax), self.qmin)

            curObj = self._ComputeObjective(targetPose, q, robotindex)
            if curObj < self.tol:
                reached = True
                break
        if not reached:
            return None
        return q


    def _ComputeDQ(self, targetPose, q, robotindex):
        robot = self.lrobot if robotindex == 0 else self.rrobot
        manip = self.lmanip if robotindex == 0 else self.rmanip
        
        with robot:
            robot.SetActiveDOFValues(q)
            Jquat = manip.CalculateRotationJacobian()
            Jtrans = manip.CalculateJacobian()            
            curPose = manip.GetTransformPose()
        if curPose[0] < 0:
            curPose[0:4] *= -1.0
            Jquat *= -1.0
        J = np.vstack([Jquat, Jtrans])
        dq = np.dot(np.linalg.pinv(J), targetPose - curPose)
        return dq


    def _ComputeObjective(self, targetPose, q, robotindex):
        robot = self.lrobot if robotindex == 0 else self.rrobot
        manip = self.lmanip if robotindex == 0 else self.rmanip
        
        with robot:
            robot.SetActiveDOFValues(q)            
            curPose = manip.GetTransformPose()
        if curPose[0] < 0:
            curPose[0:4] *= -1.0
        error = targetPose - curPose
        return np.dot(error, error)


    def _FindGraspCandidates(self, placementIndex):
        self.approachingdirs = []
        for boxinfo in self.myObject.graspinfos.values():
            for realapproachingdir in boxinfo.approachingDirections[placementIndex]:
                approachingdir = 6*boxinfo.linkIndex + realapproachingdir
                self.approachingdirs.append(approachingdir)
        log.info("placement {0}: approachingdirs = {1}".format(placementIndex, self.approachingdirs))


    def _FilterApproachingDirection(self, approachingdirection, Tobj, robotindex):
        robot = self.lrobot if robotindex == 0 else self.rrobot
        
        realapproachingdir = np.mod(approachingdirection, 6)
        graspedLinkIndex = int(approachingdirection)/6
        with self.mobj.mobj:
            self.mobj.SetTransform(Tobj)
        Tlink = self.mobj.mobj.GetLinks()[graspedLinkIndex].GetTransform()
        plink = utils.ExtractTranslationVector(Tlink)
        probot = utils.ExtractTranslationVector(robot.GetTransform())
        linkToRobotVect = utils.Normalize(plink - probot)
        # approachingVect is the approaching direction described in the world's frame
        approachingVect = Tlink[0:3, np.mod(realapproachingdir, 3)]
        if realapproachingdir > 2:
            approachingVect *= -1.0
            
        angle = np.arccos(np.dot(approachingVect, linkToRobotVect))
        if angle < self.threshangle:
            return True
        else:
            MZ = np.array([0., 0., -1.])
            angle = np.arccos(np.dot(MZ, approachingVect))
            if angle <= np.pi/3 and Tobj[2, 3] - self.myObject.Trest[2, 3] <= 0.5:
                return True
            return False

        
class BimanualSE2Planner(object):

    def __init__(self, robots, mobj, myObject):
        self.lrobot, self.rrobot = robots
        self.mobj = mobj
        self.myObject = myObject
        self.orwrapper = utils.OpenRAVEKinBodyWrapper(self.mobj, self.myObject.Tcom)        

        self.rbplanner = RigidBodyRRTPlanner()
        self.bimanualtracker = BimanualSE2MotionTracker([self.lrobot, self.rrobot], self.orwrapper, self.myObject)
        # self.ltracker = SE2MotionTracker(self.lrobot, self.orwrapper, self.myObject)
        # self.rtracker = SE2MotionTracker(self.rrobot, self.orwrapper, self.myObject)
        # assert(self.ltracker.discrTimeStep == self.rtracker.discrTimeStep)

        self._rbplannerTimeout = 3600
        

    def Solve(self, Tstart, Tgoal, Ttype='object'):
        """Return a manipulation path which brings the object from Tstart to Tgoal.

        Parameters
        ----------
        Tstart : numpy.adarray
            4x4 transformation matrix
        Tgoal : numpy.adarray
            4x4 transformation matrix
        Ttype : string
            If Ttype == 'object', the transformations are given as those of the object's 
            local frame. Otherwise, if Ttype == 'com', the transformations are given as 
            those of the frame attached to the COM.

        Returns
        -------
        manipTraj : ManipulationTrajectory
        
        """
        if Ttype == 'object':
            Ts = np.dot(Tstart, self.orwrapper.Tcom)
            Tg = np.dot(Tgoal, self.orwrapper.Tcom)
            query = SE2Query(self.mobj, Ts, Tg, self.myObject)
        else:
            query = SE2Query(self.mobj, Tstart, Tgoal, self.myObject)
            
        self.rbplanner.SetQuery(query)
        with self.mobj.GetEnv():
            self.lrobot.SetActiveDOFValues(np.zeros(6))
            self.rrobot.SetActiveDOFValues(np.zeros(6))
        solved = self.rbplanner.Plan(timeOut=self._rbplannerTimeout)
        if not solved:
            log.warn("SE(2) planning failed.")
            return
        
        se3Traj = query.GenerateFinalSE3Traj()
        shortcutSE3Traj = self.rbplanner.Shortcut(se3Traj, maxIter=500)
        ccTrajs = self.bimanualtracker.Track(shortcutSE3Traj)
        
        return self.Bridge(ccTrajs)
    

    def Bridge(self, ccTrajs):
        """Plan transit trajectories needed to bridge the gaps between consecutive cc trajectories.

        Parameters
        ----------
        ccTrajs : list
            List of closed-chain trajectories

        """
        nPlanningTrials = 5
        
        manipTraj = ManipulationTrajectory()
        manipTraj.AddTrajectory(ccTrajs[0], manipTraj.TRANSFER)
        for ccTraj in ccTrajs[1:]:
            Tobj = ccTraj.se3_traj.Eval(0)
            qlstart = manipTraj.trajsList[-1].bimanual_wpts[0][-1]
            qlgoal = ccTraj.bimanual_wpts[0][0]
            qrstart = manipTraj.trajsList[-1].bimanual_wpts[1][-1]
            qrgoal = ccTraj.bimanual_wpts[1][0]
            self.orwrapper.SetTransform(Tobj)
            lRegrasping = True
            rRegrasping = True
            ldiff = qlstart - qlgoal
            if np.dot(ldiff, ldiff) < 1e-6:
                lRegrasping = False
            rdiff = qrstart - qrgoal
            if np.dot(rdiff, rdiff) < 1e-6:
                rRegrasping = False
            assert(lRegrasping or rRegrasping)
            
            if lRegrasping and rRegrasping:
                # Plan transit trajectory for lrobot
                self.rrobot.SetActiveDOFValues(qrstart)
                lTraj = mp.plan_transit_motion(self.lrobot, qlstart, qlgoal, True, True, nPlanningTrials)
                if lTraj is None:
                    import IPython; IPython.embed(header="planning transit path failed (L).")
                    return None
                # Plan transit trajectory for rrobot
                self.lrobot.SetActiveDOFValues(qlgoal)
                rTraj = mp.plan_transit_motion(self.rrobot, qrstart, qrgoal, True, True, nPlanningTrials)
                if rTraj is None:
                    import IPython; IPython.embed(header="planning transit path failed (R).")
                    return None

            elif lRegrasping:
                # Plan transit trajectory for lrobot
                self.rrobot.SetActiveDOFValues(qrgoal)
                lTraj = mp.plan_transit_motion(self.lrobot, qlstart, qlgoal, True, True, nPlanningTrials)
                if lTraj is None:
                    import IPython; IPython.embed(header="planning transit path failed (L).")
                    return None
                rTraj = None

            elif rRegrasping:
                # Plan transit trajectory for rrobot
                self.lrobot.SetActiveDOFValues(qlgoal)
                rTraj = mp.plan_transit_motion(self.rrobot, qrstart, qrgoal, True, True, nPlanningTrials)
                if rTraj is None:
                    import IPython; IPython.embed(header="planning transit path failed (R).")
                    return None
                lTraj = None

            transitTraj = TransitTrajectory(lTraj=lTraj, rTraj=rTraj)
            manipTraj.AddTrajectory(transitTraj, manipTraj.TRANSIT)

            manipTraj.AddTrajectory(ccTraj, manipTraj.TRANSFER)

        return manipTraj


class BimanualSE2PlannerWithPaddedObject(object):
    """In rigid body motion planning, use normal object. In tracking, use padded object.

    """
    
    def __init__(self, robots, mobj, myObject, paddedObject):
        self.lrobot, self.rrobot = robots
        self.mobj = mobj
        self.padded = paddedObject
        self.myObject = myObject
        self.orwrapper = utils.OpenRAVEKinBodyWrapper(self.mobj, self.myObject.Tcom)
        self.paddedOrwrapper = utils.OpenRAVEKinBodyWrapper(self.padded, self.myObject.Tcom)

        self.rbplanner = RigidBodyRRTPlanner()
        self.bimanualtracker = BimanualSE2MotionTracker([self.lrobot, self.rrobot], self.paddedOrwrapper, self.myObject)
        # self.ltracker = SE2MotionTracker(self.lrobot, self.orwrapper, self.myObject)
        # self.rtracker = SE2MotionTracker(self.rrobot, self.orwrapper, self.myObject)
        # assert(self.ltracker.discrTimeStep == self.rtracker.discrTimeStep)

        self._rbplannerTimeout = 3600


    def EnablePadded(self, enabled=True):
        self.padded.SetVisible(enabled)
        self.padded.Enable(enabled)
        # Don't forget that we want to enable/disable only the mesh
        self.mobj.GetLinks()[0].SetVisible(not enabled)
        self.mobj.GetLinks()[0].Enable(not enabled)

        if enabled:
            self.myObject.mobj = self.padded
        else:
            self.myObject.mobj = self.mobj
            

    def Solve(self, Tstart, Tgoal, Ttype='object'):
        """Return a manipulation path which brings the object from Tstart to Tgoal.

        Parameters
        ----------
        Tstart : numpy.adarray
            4x4 transformation matrix
        Tgoal : numpy.adarray
            4x4 transformation matrix
        Ttype : string
            If Ttype == 'object', the transformations are given as those of the object's 
            local frame. Otherwise, if Ttype == 'com', the transformations are given as 
            those of the frame attached to the COM.

        Returns
        -------
        manipTraj : ManipulationTrajectory
        
        """
        self.EnablePadded(False)
        
        if Ttype == 'object':
            Ts = np.dot(Tstart, self.orwrapper.Tcom)
            Tg = np.dot(Tgoal, self.orwrapper.Tcom)
            query = SE2Query(self.mobj, Ts, Tg, self.myObject)
        else:
            query = SE2Query(self.mobj, Tstart, Tgoal, self.myObject)
            
        self.rbplanner.SetQuery(query)
        with self.mobj.GetEnv():
            self.lrobot.SetActiveDOFValues(np.zeros(6))
            self.rrobot.SetActiveDOFValues(np.zeros(6))
        solved = self.rbplanner.Plan(timeOut=self._rbplannerTimeout)
        if not solved:
            log.warn("SE(2) planning failed.")
            return

        self.EnablePadded(True)
        se3Traj = query.GenerateFinalSE3Traj()
        shortcutSE3Traj = self.rbplanner.Shortcut(se3Traj, maxIter=500)
        ccTrajs = self.bimanualtracker.Track(shortcutSE3Traj)
        self.EnablePadded(False)
        
        return self.Bridge(ccTrajs)
    

    def Bridge(self, ccTrajs):
        """Plan transit trajectories needed to bridge the gaps between consecutive cc trajectories.

        Parameters
        ----------
        ccTrajs : list
            List of closed-chain trajectories

        """
        self.EnablePadded(True)
        
        nPlanningTrials = 5
        
        manipTraj = ManipulationTrajectory()
        manipTraj.AddTrajectory(ccTrajs[0], manipTraj.TRANSFER)
        for ccTraj in ccTrajs[1:]:
            Tobj = ccTraj.se3_traj.Eval(0)
            qlstart = manipTraj.trajsList[-1].bimanual_wpts[0][-1]
            qlgoal = ccTraj.bimanual_wpts[0][0]
            qrstart = manipTraj.trajsList[-1].bimanual_wpts[1][-1]
            qrgoal = ccTraj.bimanual_wpts[1][0]
            self.paddedOrwrapper.SetTransform(Tobj)
            lRegrasping = True
            rRegrasping = True
            ldiff = qlstart - qlgoal
            if np.dot(ldiff, ldiff) < 1e-6:
                lRegrasping = False
            rdiff = qrstart - qrgoal
            if np.dot(rdiff, rdiff) < 1e-6:
                rRegrasping = False
            assert(lRegrasping or rRegrasping)
            
            if lRegrasping and rRegrasping:
                # Plan transit trajectory for lrobot
                self.rrobot.SetActiveDOFValues(qrstart)
                lTraj = mp.plan_transit_motion(self.lrobot, qlstart, qlgoal, True, True, nPlanningTrials)
                if lTraj is None:
                    import IPython; IPython.embed(header="planning transit path failed (L).")
                    return None
                # Plan transit trajectory for rrobot
                self.lrobot.SetActiveDOFValues(qlgoal)
                rTraj = mp.plan_transit_motion(self.rrobot, qrstart, qrgoal, True, True, nPlanningTrials)
                if rTraj is None:
                    import IPython; IPython.embed(header="planning transit path failed (R).")
                    return None

            elif lRegrasping:
                # Plan transit trajectory for lrobot
                self.rrobot.SetActiveDOFValues(qrgoal)
                lTraj = mp.plan_transit_motion(self.lrobot, qlstart, qlgoal, True, True, nPlanningTrials)
                if lTraj is None:
                    import IPython; IPython.embed(header="planning transit path failed (L).")
                    return None
                rTraj = None

            elif rRegrasping:
                # Plan transit trajectory for rrobot
                self.lrobot.SetActiveDOFValues(qlgoal)
                rTraj = mp.plan_transit_motion(self.rrobot, qrstart, qrgoal, True, True, nPlanningTrials)
                if rTraj is None:
                    import IPython; IPython.embed(header="planning transit path failed (R).")
                    return None
                lTraj = None

            transitTraj = TransitTrajectory(lTraj=lTraj, rTraj=rTraj)
            manipTraj.AddTrajectory(transitTraj, manipTraj.TRANSIT)

            manipTraj.AddTrajectory(ccTraj, manipTraj.TRANSFER)

        self.EnablePadded(False)
        return manipTraj



