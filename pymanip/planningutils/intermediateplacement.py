import openravepy as orpy
import numpy as np
from copy import deepcopy
import scipy as sp
from scipy.spatial import ConvexHull
from cdd import RepType
from cone import FractionToFloatArray
import cone
import cvxopt
import trimesh
import time
import random
import IPython

from . import utils, myobject
from . import staticequilibrium as eq
from . import staticequilibrium_lp as eq2

import logging
logging.basicConfig(format='[%(levelname)s] [%(name)s: %(funcName)s] %(message)s', level=logging.DEBUG)
log = logging.getLogger(__name__)

# Important notes: stability result differs by pretty much when changing this _decimals. Part of it
# may be because we are mainly using 'float' option when computing cdd (faster but less precise).
_decimals = 10 # for np.around

_rng = random.SystemRandom()

X = np.array([1., 0., 0.])
Y = np.array([0., 1., 0.])
Z = np.array([0., 0., 1.])


def ComputeClosePlacements(myObject, Tcur, Trest, placementType=0, nPlacements=1, returnLocalContactPoints=False):
    """Given the current object transformation Tcur and the resting surface transformation
    Trest, find a close transformation T for the object such that the object is resting on
    the resting surface.

    placementType indicates the type of placement the function should compute. Available
    options are 0:surface and 1:edge.

    Important note: In the current implementation, we assume that the floor is horizontal.
    """
    # Compute the relative transformation from the resting surface to the object's frame
    Trel = reduce(np.dot, [np.linalg.inv(Trest), Tcur, myObject.Tcom])

    # Since the object's local frame and the frame attached to the COM have the same
    # rotational component, whether or not we include myObject.Tcom in the above formula
    # does not affect the resulting transformation in the end.

    # We create a new mesh, for computing AABB, aligning with the resting surface's
    # frame. Note that myObject.vertices contains vertices of the object's convex hull and
    # all the vertices are described in the frame attached to the COM of the object.
    newVertices = [utils.Transform(v, Trel) for v in myObject.vertices]

    """
    # Script for adding the AABB kinbody to the environment
    newMesh = trimesh.Trimesh(vertices=newVertices, faces=myObject.faces)
    newMesh.fix_normals()
    aabbExtents = newMesh.bounding_box.extents # dimension of the AABB
    bb = orpy.RaveCreateKinBody(env, '')
    bb.InitFromBoxes(np.array([[0, 0, 0, aabbExtents[0]/2, aabbExtents[1]/2, aabbExtents[2]/2]]))
    bb.SetName('bb')
    bb.GetLinks()[0].GetGeometries()[0].SetTransparency(0.5)
    env.Add(bb)
    """

    # Sort newVertices according to the Z-component of each vertex (to find out which one is
    # touching the floor first)
    sortedcoords, sortedindices = (list(t) for t in zip(*sorted(zip([v[2] for v in newVertices], range(len(newVertices))))))

    # This is the facet that will for sure have to touch the floor
    index0 = sortedindices[0]

    if placementType == 0:
        # Here the object is to be placed on a facet. We first extract all facets which
        # contain newVertices[index0]. Note that newVertices[i] and myObject.vertices[i]
        # are referring to the same vertex, only expressed in different coordinates.

        candidateFacets = [] # contains all facets containing newVertices[index0]
        for (ifacet, faces) in myObject.facets.iteritems():
            # Collect all vertices forming this facet
            allVertices = []
            for iface in faces:
                allVertices += myObject.faces[iface].tolist()
            if (index0 in allVertices):
                candidateFacets.append(ifacet)
        assert(len(candidateFacets) > 0)

        nPlacements = min(nPlacements, len(candidateFacets))

        facetRanking = []
        for ifacet in candidateFacets:
            normal = utils.Rotate(myObject.planes[ifacet][0:3], Trel)
            angle = np.arccos(-np.dot(normal, Z))
            facetRanking.append((ifacet, abs(angle)))

        sortedelements, sortedindices2 = (list(t) for t in zip(*sorted(zip([e[1] for e in facetRanking], range(len(facetRanking))))))
        """
        # Script to visualize the closest facet
        closestFacet = facetRanking[sortedindices2[0]][0]
        allv = []
        for iface in myObject.facets[closestFacet]:
            allv += myObject.faces[iface].tolist()
        allv = list(set(allv)) # remove redundant indices
        h = [] # plot handles
        for index in allv:
            v = utils.Transform(myObject.vertices[index], Trel)
            h.append(env.plot3(points=v, pointsize=10, colors=[1, 0, 0]))
        """
        Tlist = []

        for sortedindex in sortedindices2[0:nPlacements]:
            ifacet = facetRanking[sortedindex][0]

            # Describe the normal vector of the facet in the frame attached to the floor.
            normal = utils.Rotate(myObject.planes[ifacet][0:3], Trel)

            # Next we compute a close object transformation at which the normal is aligned
            # with the floor's -Z axis.
            # See http://math.stackexchange.com/questions/180418/calculate-rotation-matrix-to-align-vector-a-to-vector-b-in-3d
            c = np.cross(Z, normal) # crossProduct = normal x -Z
            d = -np.dot(normal, Z)  # dotProduct = normal^T(-Z)
            cx = utils.ComputeSkewFromVect(c)
            R = np.eye(3) + cx + np.dot(cx, cx)/(1. + d) # rotation matrix which aligns normal with -Z
            T = utils.CreateTransformFromRotation(R)

            Tcur_rotation = utils.ExtractRotation(Tcur)
            # Tint is an intermediate transformation where the closest facet is already
            # parallel to the floor
            Tint = np.dot(T, Tcur_rotation)
            Tint[0:3, 3] = utils.ExtractTranslationVector(Tcur) # put back the translational part

            # Now move the object toward the floor
            Ttrans = utils.ComputeTTrans(2, -(Tint[2, 3] - myObject.planes[ifacet][3] + np.dot(utils.ExtractRotation(Tint), myObject.Tcom)[2, 3]))
            Tfinal = np.dot(Ttrans, Tint)
            Tlist.append(np.dot(Trest, Tfinal))
        return Tlist
    
    elif placementType == 1:
        # Here the object is to be placed on an edge
        
        index1 = sortedindices[1]

        # Look for any edge containing at least one of the two vertices with the
        # smallest Z-coordinates.
        candidateEdges = []
        for (iedge, vCommon) in myObject.edges.iteritems():
            if (index0 in vCommon) or (index1 in vCommon):
                candidateEdges.append(iedge)

        assert(len(candidateEdges) > 0)

        nPlacements = min(nPlacements, len(candidateEdges))

        edgeRanking = []
        halfPi = 0.5*np.pi
        for iedge in candidateEdges:
            iv0, iv1 = myObject.edges[iedge]
            v0, v1 = [newVertices[index] for index in [iv0, iv1]]
            direction = utils.Normalize(v1 - v0)
            # angle indicates how close the edge is to the floor. The smaller the angle,
            # the closer to the floor.
            angle = halfPi - abs(np.arccos(np.dot(direction, Z)))
            edgeRanking.append((iedge, angle))
            
        sortedelements, sortedindices2 = (list(t) for t in zip(*sorted(zip([e[1] for e in edgeRanking], range(len(edgeRanking))))))

        Tlist = []
        
        for sortedindex in sortedindices2[0:nPlacements]:
            iedge = edgeRanking[sortedindex][0]
            i0, i1 = myObject.edges[iedge]
            # v0 and v1 are already expressed in the resting surface frame
            v0 = newVertices[i0]
            v1 = newVertices[i1]
            if v0[2] > v1[2]:
                v0 = newVertices[i1]
                v1 = newVertices[i0]

            direction = utils.Normalize(v1 - v0)
            projectedVect = utils.Normalize(np.array([direction[0], direction[1], 0]))

            c = np.cross(direction, projectedVect)
            d = np.dot(direction, projectedVect)
            cx = cx = utils.ComputeSkewFromVect(c)
            # R is the rotation matrix which aligns direction with projectedVect
            R = np.eye(3) + cx + np.dot(cx, cx)/(1. + d)
            T = utils.CreateTransformFromRotation(R)

            Tcur_rotation = utils.ExtractRotation(Tcur)
            # Tint is an intermediate transformation where the closest facet is already
            # parallel to the floor
            Tint = np.dot(T, Tcur_rotation)
            Tint[0:3, 3] = utils.ExtractTranslationVector(Tcur) # put back the translational part

            # Now move the object toward the floor.

            # v0new is the vertex myObject.vertices[i0] expressed in the resting surface
            # frame while the object is at Tint.
            v0new = utils.Transform(myObject.vertices[i0], np.dot(Tint, myObject.Tcom))
            
            Ttrans = utils.ComputeTTrans(2, -v0new[2])
            Tfinal = np.dot(Ttrans, Tint)

            # Sometimes, the calculated Tfinal is such that the desired edge touches the
            # floor but some other parts of the object actually submerge into the
            # floor. In that case, when computing qobj (qobj = [iedge, alpha, x, y,
            # theta]), we will find out that alpha < 0. We fix this case by inverting
            # (negating alpha).
            qobj = myObject.ComputeQObjectOnEdge(Tfinal, Trest=Trest, forRealObject=True)
            if qobj[1] < 0:
                qobj[1] *= -1
                Tfinal = myObject.ComputeTObjectOnEdge(qobj, Trest=Trest, forRealObject=True)
            
            Tlist.append(np.dot(Trest, Tfinal))
        return Tlist

    elif placementType == 2:
        # Here the object is to be placed on a vertex

        # Since we do not have any way (yet) to parameterize this kind of placements, we
        # will (for now) always return one configuration which is Tcur translated along
        # the resting surface's Z axis such that the first point touches the floor.
        v0 = newVertices[index0]
        Ttrans = utils.ComputeTTrans(2, -v0[2])
        Tfinal = reduce(np.dot, [Trest, Ttrans, np.linalg.inv(Trest), Tcur])
        if returnLocalContactPoints:
            return [(Tfinal, [myObject.vertices[index0]])]
        else:
            return [Tfinal]
        
    else:
        log.warn("placementType = {0} not implemented".format(placementType))
        raise NotImplementedError
        

def ComputeFeasibleClosePlacements(robots, qgrasps, mobj, Tobj, Trest, fmax, mu, placementType=0, myObject=None):
    """Given the current transformation of the object, Tobj, try to find an object
    transformation such that
    
    1. The object is placed on the table with the specified placementType (0: surface, 1:
    edge, 2: vertex). Currently only type2 is properly working.

    2. The object will be in static equilibrium regardless of which robot is holding the
    object.

    """
    Tfeas = None
    contactPoints = None
    lrobot, rrobot = robots
    env = lrobot.GetEnv()
    qgrasp_l, qgrasp_r = qgrasps
    lmanip, rmanip = [robot.GetActiveManipulator() for robot in robots]
    lcontroller, rcontroller = [robot.GetController() for robot in robots]
    ltaskmanip = orpy.interfaces.TaskManipulation(lrobot)
    rtaskmanip = orpy.interfaces.TaskManipulation(rrobot)

    lrobot.SetActiveDOFs(xrange(6))
    rrobot.SetActiveDOFs(xrange(6))
    lrobot.SetActiveDOFValues(np.zeros(7))
    rrobot.SetActiveDOFValues(np.zeros(7))
    
    if myObject is None:
        myObject = myobject.MyObject(mobj)

    """Possible approaches to generating a list of transformation candidates

    1. Use ComputeClosePlacements to generate a list of nominal transformations from
    appropriate placement classes [T1, T2, ..., Tn]. Then for each Ti, if Ti is not
    feasible, try different values of placement parameters (in the same class). If this
    still does not work (for at least a few times), start trying with T(i + 1).

    2. From Tcur, generate some randomly perturbed object configurations [T1, T2, ...,
    Tn]. Then for each Ti, generate one placement configuration by translating the object
    along the floor's Z axis until the first point touches the floor. (i.e., considering
    only placements on vertices).

    """
    approach = 2
    if approach == 1:
        pass
    elif approach == 2:
        magnitude_rot = 0.3
        magnitude_trans = 0.1
        nCandidates = 20
        Tlist = [PerturbTransformation(Tobj, magnitude_rot, magnitude_trans) for _ in xrange(nCandidates)]
        Tlist.insert(0, Tobj)

        candidateList = [ComputeClosePlacements(myObject, T, Trest, placementType=2, returnLocalContactPoints=True)[0] for T in Tlist]

        # TO BE REMVOED
        for candidate in candidateList:
            T = candidate[0]
            T[0, 3] -= 0.15
            
        iksols = []

        for candidate in candidateList:
            T = candidate[0]
            # Now check feasibility of each candidate transformation
            mobj.SetTransform(T)        
            Tgripper_l = utils.ComputeTGripper2(mobj, qgrasp_l[0], qgrasp_l)
            Tgripper_r = utils.ComputeTGripper2(mobj, qgrasp_r[0], qgrasp_r)

            # Do not forget to open fingers
            lrobot.SetDOFValues([0], [6])
            rrobot.SetDOFValues([0], [6])
            sols_l = lmanip.FindIKSolutions(Tgripper_l, orpy.IkFilterOptions.CheckEnvCollisions)
            sols_r = rmanip.FindIKSolutions(Tgripper_r, orpy.IkFilterOptions.CheckEnvCollisions)

            if (len(sols_l) == 0) or (len(sols_r) == 0):
                continue

            iksols = [sols_l, sols_r]

            lrobot.SetActiveDOFValues(sols_l[0])
            rrobot.SetActiveDOFValues(sols_r[0])
            ltaskmanip.CloseFingers()
            rtaskmanip.CloseFingers()
            while (not lcontroller.IsDone()) or (not lcontroller.IsDone()):
                time.sleep(0.001)

            # Now check static equilibrium
            # First transform contact points into the global frame
            temp = [utils.Transform(point, T) for point in candidate[1]]
            result = eq2.CheckStaticEquilibrium(robots, qgrasps, mobj, Tobj, Trest, fmax, mu, myObject, otherContactPoints=temp, checkSingleRobot=True)            
            
            if result[0] and result[1]:
                Tfeas = T
                contactPoints = candidate[1]
                break
            else:
                continue

    # ltaskmanip.ReleaseFingers()
    # rtaskmanip.ReleaseFingers()
    # while (not lcontroller.IsDone()) or (not lcontroller.IsDone()):
    #     time.sleep(0.001)
    # A faster way of releasing fingers
    lrobot.SetDOFValues([0], [6])
    rrobot.SetDOFValues([0], [6])
    return Tfeas, iksols, contactPoints


def PerturbTransformation(Tcur, magnitude_rot, magnitude_trans, checkCollision=False, mobj=None):
    """Perturb the given transformation by sampling two (3 dimensional) noise vectors, one for
    rotation and another for translation.

    If checkCollision is to be set to True, the user needs to input the manipulated object
    as well.

    This function returns a purterbed transformation matrix.
    """
    R = np.array(Tcur[0:3, 0:3])
    w = utils.ComputeLogVector(R)
    p = np.array(Tcur[0:3, 3])

    env = None
    if mobj is not None:
        env = mobj.GetEnv()
    
    passed = False
    while not passed:
        wnew = [val + (_rng.uniform(-1, 1)*magnitude_rot) for val in w]
        pnew = [val + (_rng.uniform(-1, 1)*magnitude_trans) for val in p]
        Rnew = utils.ComputeExponentMatrix(wnew)
        Tnew = utils.CombineRotationTranslation(Rnew, np.array(pnew))
        if checkCollision:
            if env is not None:
                with mobj:
                    mobj.SetTransform(Tnew)
                    passed = not env.CheckCollision(mobj)
            else:
                raise Exception("checkCollision is enabled but no mobj provided.")
        else:
            passed = True            
        
    return Tnew

    
def CreateOverlayBodies(Tlist, mobj, transparency=0.8):
    """CreateOverlayBodies visualizes all the transformations in the given Tlist by creating
    overlaying bodies of mobj in the scene.

    The purpose of this function is to visualized perturbed transformations generated by
    the function PerturbTransformation.
    """
    if transparency < 0:
        transparency *= -1
    if transparency > 1:
        transparency = 1./transparency

    env = mobj.GetEnv()
    newObjects = []
    with env:
        for T in Tlist:
            newObject = orpy.RaveCreateKinBody(env, mobj.GetXMLId())
            newObject.Clone(mobj, 0)
            newObject.SetName(mobj.GetName())
            for link in newObject.GetLinks():
                for geom in link.GetGeometries():
                    geom.SetTransparency(transparency)
            env.Add(newObject, True)
            newObject.SetTransform(T)
            newObjects.append(newObject)
    return newObjects
    
