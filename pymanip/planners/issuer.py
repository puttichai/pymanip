# -*- coding: utf-8 -*-
#
# Copyright (C) 2016 Puttichai Lertkultanon <L.Puttichai@gmail.com>
#
# This file is part of pymanip.
#
# pymanip is free software: you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# pymanip is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# pymanip. If not, see <http://www.gnu.org/licenses/>.

"""The planner constructs and solve closed-chain queries which connect different placement
classes. The set of solutions, i.e., transfer paths connecting placement classes, is
called a certificate which guarantees the existence of solutions to any query.

A certificate can be used later by querysolver to help construct solutions to queries.

"""

import openravepy as orpy
import numpy as np
import networkx as nx
import logging
import random
import time
import itertools

from bimanual.planners import cc_planner_se3 as ccp # to be fixed later in bimanual
from bimanual.utils import loggers
from ..planningutils import utils
from ..planningutils import graspequilibrium as ge

loglevel = logging.DEBUG
logging.basicConfig(format='[%(levelname)s] [%(name)s: %(funcName)s] %(message)s', level=loglevel)
log = logging.getLogger(__name__)
rng = random.SystemRandom()
ikfilter = orpy.IkFilterOptions.CheckEnvCollisions

PX = np.array([ 1.,  0.,  0.])
MX = np.array([-1.,  0.,  0.])
MZ = np.array([ 0.,  0., -1.])


class Issuer(object):
    """Generate and solve closed-chain queries which connect different placement classes. The
    set of solutions, i.e., the set of transfer trajectories, is called a certificate.

    """

    def __init__(self, robots, mobj, myObject):
        self.lrobot, self.rrobot = robots
        self.mobj = mobj # movable object
        self.myObject = myObject # MyObject

        self.lmanip = self.lrobot.GetActiveManipulator()
        self.rmanip = self.rrobot.GetActiveManipulator()
        self.ltaskmanip = orpy.interfaces.TaskManipulation(self.lrobot)
        self.rtaskmanip = orpy.interfaces.TaskManipulation(self.rrobot)
        self.env = self.mobj.GetEnv() # OpenRAVE environment
        self.orkinbodywrapper = OpenRAVEKinBodyWrapper(self.mobj, self.myObject.Tcom)
        self.ccplanner = ccp.CCPlanner(self.orkinbodywrapper, robots, planner_type='BiRRT',
                                       logger=loggers.TextColors(1))

        # placementconnection keeps track of computed connections between placement
        # classes
        self.placementconnection = nx.Graph()
        self.placementconnection.add_nodes_from(self.myObject.agStable.nodes())
        self.successful = False

        self._controllersleeptime = 0.001
        self._homeconfig = np.zeros(self.lrobot.GetActiveDOF())
        self._threshangle = np.pi * 0.45 # 

        # fmax is the maximum force that a gripper can exwer on an object. If we assume
        # that the gripper can exert arbitrarily large force, then put None.
        self._fmax = None
        self._mu = 0.5 # static friction coefficient


    def ComputeCertificate(self, nominalPosition, earlytermination=False):
        """Compute a certificate which is a set of transfer paths connecting different placement
        classes.

        First, it picks n_p - 1 pairs of placement classes, where n_p is the number of
        total placement classes, by generating a minimum planning tree on the placement
        adjacency graph. Then for each pair of placement classes, closed-chain queries are
        generated and solved (by self.Verify).

        The user has an option to terminate this certificate computation right after a set
        of n_p - 1 transfer paths has been generated. Otherwise, the planner can continue
        to generate transfer paths connecting the remaining pair of placement classes.

        Parameters
        ----------
        nominalPosition : list or numpy.ndarray of length 2
            2D position, i.e., (x, y), of the object placement parameters. The initial and
            goal transformations for closed-chain queries will be generated in the
            neighborhood of this point.

        Returns
        -------
        result : boolean
            True if certificate computation is successful (there is one connected component
            in the graph). False, otherwise.
        """
        
        graph = nx.Graph()
        nodes = self.myObject.agStable.nodes()
        graph.add_nodes_from(nodes)
        # Create a complete graph (having an edge connecting each pair of nodes)
        while len(nodes) > 0:
            node = nodes.pop()
            for remainingnode in nodes:
                graph.add_edge(node, remainingnode)
        edges = graph.edges()
        ntransfers = 0
        # Iterate over all edges and plan a transfer trajectory for every pair of placement classes
        for edge in edges:
            ifacet0, ifacet1 = edge # unpack the facet indices
            log.info("\nVerifying connection between facet {0} and {1}\n".format(ifacet0, ifacet1))
            passed = self.Verify(ifacet0, ifacet1, nominalPosition)
            if passed:
                ntransfers += 1
        log.info("Transfer paths successfully planned: {0} out of {1}".format(ntransfers, len(graph.edges())))

        # A certificate computation is successful is there is only one connected component in placementconnection
        comps = [c for c in nx.connected_components(self.placementconnection)]
        if len(comps) == 1:
            self.successful = True
            return True
        else:
            return False


    def Save(self, filename):
        if not self.successful:
            log.info("Certificate computation is not successful (yet).")
            cont = raw_input("Do you wish to continue to save the result? [Y/n]")
            if not (cont.lower() in ["", "y"]):
                return False

        import pickle
        data = self.placementconnection.edges(data=True)
        # my_object cannot be pickled.
        for d in data:
            d[-1]['certificate'].my_object = None
        try:
            with open(filename, 'wb') as f:
                pickle.dump(data, f, pickle.HIGHEST_PROTOCOL)
            return True
        except:
            log.warn("Exception occured when saving the result")
            return False
                    

    def Verify(self, ifacet0, ifacet1, nominalPosition):
        """Given two facet indices: ifacet0 and ifacet1, this function generates and sovles
        closed-chain queries which connect the two placement classes.

        Note that facet index and placement class index are equivalent since a placement
        class corresponds to one and only one facet of the convex hull.
        
        Parameters
        ----------
        ifacet0 : int
            Placement class index
        ifacet1 : int
            Placement class index
        nominalPosition : list or numpy.ndarray of length 2
            2D position, i.e., (x, y), of the object placement parameters. The initial and
            goal transformations for closed-chain queries will be generated in the
            neighborhood of this point.

        """
        # CCPlanner parameters
        stepSize = 0.1
        velMult  = 1.0
        enableBW = True
        timeOut  = 60
        passed   = False
        ntrials  = 25
        # Noise magnitudes to be used in perturbing the transformations
        # dx     = 0.1 # 10 cm.
        dx     = 0.3
        dy     = 0.1 # 10 cm.
        dtheta = np.pi/18 # 10 degree

        # Statistics
        nCCPlannerFailed = 0
        nGraspEqFailed   = 0

        ts = time.time()
        [qobj0, qobj1], [T0, T1] = self.GenerateTransformations(ifacet0, ifacet1, nominalPosition)
        for i in xrange(ntrials):
            if i == 0:
                Tstart, Tgoal = T0, T1
                qobjStart, qobjGoal = qobj0, qobj1
            else:
                # Perturb T0 and T1
                qobjStart, Tstart = self.PerturbPlacement(qobj0, dx, dy, dtheta)
                qobjGoal, Tgoal = self.PerturbPlacement(qobj1, dx, dy, dtheta)

            # Sample grasps for closed-chain planning
            qgrasps, lsols, rsols = self.SampleGrasps(Tstart, Tgoal, qobjStart, qobjGoal)
            if len(lsols) == 0:
                # Grasp sampling failed. This may be becasue Tstart and Tgoal have no
                # common grasp class or available IK solutions at Tstart and Tgoal are not
                # likely to be connected by closed-chain motions.
                continue

            # Compute the COM transformation at the start and goal
            Tcom_start = np.dot(Tstart, self.myObject.Tcom)
            Tcom_goal = np.dot(Tgoal, self.myObject.Tcom)
            objLowerLimits, objUpperLimits = self.GenerateObjectTranslationalLimits\
                                             (Tcom_start, Tcom_goal)
            # Generate a closed-chain query
            ccquery = ccp.CCQuery(self.myObject, [objUpperLimits, objLowerLimits], [lsols[0], rsols[0]], [lsols[1], rsols[1]], [0, 0], Tcom_start, nn=15, step_size=stepSize, velocity_scale=velMult, enable_bw=enableBW)
            res = self.ccplanner.set_query(ccquery)
            if not res:
                # We have problem setting the query. Most likely the start/goal configurations are in collision.
                continue
            # Solve the query
            solved = self.ccplanner.solve(timeout=timeOut)
            if not solved:
                nCCPlannerFailed += 1
                continue

            te = time.time()
            planningtime = te - ts

            ts = time.time()
            # Check grasp equilibrium along the trajectory. (yes, this could have been
            # done during the planning.)
            grasp_passed = ge.CheckGraspEquilibriumAlongTrajectory(ccquery.cctraj, [self.lrobot, self.rrobot], self.mobj, qgrasps, self.myObject, self._fmax, self._mu, taskmanips=[self.ltaskmanip, self.rtaskmanip])
            if not grasp_passed:
                nGraspEqFailed += 1
                continue
            te = time.time()
            eqcheckingtime = te - ts
            
            ts = time.time()
            self.ccplanner.shortcut(ccquery, maxiter=200)
            te = time.time()
            shortcuttime = te - ts
            # Add an edge to placementconnection graph. The edge keeps record of the
            # planned transfer trajectory
            self.placementconnection.add_edge\
                (ifacet0, ifacet1, certificate=self.ccplanner._query, qgrasps=qgrasps,
                 planningtime=planningtime, eqcheckingtime=eqcheckingtime, shortcuttime=shortcuttime,
                 nCCPlannerFailed=nCCPlannerFailed, nGraspEqFailed=nGraspEqFailed)
            return True

        log.warn(utils.Colorize("Failed to verify {0}-{1} connection".format(ifacet0, ifacet1), 'yellow'))
        print "T0 = np.{0}\nT1 = np.{1}".format(repr(T0), repr(T1))
        return False


    def GenerateObjectTranslationalLimits(self, TcomStart, TcomGoal):
        """Compute a translational limits, lowerlimits and uppoerlimits \in R^3, to be used by the
        closed-chain planner. Well specified limits will help the planner generate better
        samples.
        
        Parameters
        ----------
        TcomStart : numpy.ndarray
            Homogeneous transformation matrix
        TcomGoal : numpy.ndarray
            Homogeneous transformation matrix
        
        """
        pStart = utils.ExtractTranslationVector(TcomStart)
        pGoal = utils.ExtractTranslationVector(TcomGoal)

        vectMin = np.vectorize(min)
        vectMax = np.vectorize(max)
        lowerLimits = vectMin(pStart, pGoal)
        upperLimits = vectMax(pStart, pGoal)
        # Idea: use the maximum of the COM's z-coordinate at all placement as a base value.
        zValues = []
        for ifacet in self.myObject.stablefacetindices:
            qobj = [ifacet, 0, 0, 0]
            Tobj = self.myObject.ComputeTObject(qobj, forRealObject=False)
            zValues.append(Tobj[2, 3])
        upperLimits[2] = max(zValues)

        # Extend the current boundary by some amount
        lowerLimits[0] -= 0.2
        lowerLimits[1] -= 0.1
        upperLimits[0] += 0.2
        upperLimits[1] += 0.1
        upperLimits[2] += 0.2
        print "lowerlimits = np.{0}\nupperlimits = np.{1}".format(repr(lowerLimits), repr(upperLimits))
        return [lowerLimits, upperLimits]


    def GenerateTransformations(self, ifacet0, ifacet1, nominalPosition):
        """Generate the start and goal transformations to be used as a closed-chain query.

        Note: ***

        Parameters
        ----------
        ifacet0 : int
            Placement class index
        ifacet1 : int
            Placement class index
        nominalPosition : list or numpy.ndarray of length 2
            2D position, i.e., (x, y), of the object placement parameters. The initial and
            goal transformations for closed-chain queries will be generated in the
            neighborhood of this point.
        
        """
        #
        # Compute T0
        #
        
        # Generate the first guess of T0
        qobj0 = [ifacet0, nominalPosition[0], nominalPosition[1], 0]
        T0 = self.myObject.ComputeTObject(qobj0)

        # Then rotate T0 such that the nextNormal is aligned with the world's +X
        nextNormal1_local = self.myObject.planes[ifacet1][0:3]
        # nextNormal1 is the normal vector pointing out of facet ifacet1, expressed in the
        # world's frame
        nextNormal1 = utils.Rotate(nextNormal1_local, T0)
        projectedNormal1 = utils.Normalize(utils.Project(nextNormal1, 2)) # X-Y plane projection
        theta1 = np.arccos(np.dot(projectedNormal1, PX))
        # Now select which direction to rotate the object so as to align the normal to +X
        if projectedNormal1[1] >= 0:
            qobj0[-1] = theta1
        else:
            qobj0[-1] = -theta1
        T0 = self.myObject.ComputeTObject(qobj0)

        #
        # Compute T1
        #
        
        # See how much we need to rotate (flip) the object such that it lands on facet
        # ifacet1.
        nextNormal2 = utils.Rotate(nextNormal1_local, T0) # update the normal vector
        assert(abs(nextNormal2[1]) <= 1e-6) # check the alignment of the normal with X axis
        theta2 = np.arccos(np.dot(nextNormal2, MZ))
        # Rotate the object around Y-axis for +theta2 rad (because we assume flipping
        # backward motion for now)
        Troty = utils.ComputeTRot(1, theta2)
        Ttrans = utils.ExtractTranslation(T0)
        # Move the object back to origin, rotate, move it back. Now T1 will be such
        # that the normal of facet ifacet1 is pointing toward -Z.
        T1 = reduce(np.dot, [Ttrans, Troty, np.linalg.inv(Ttrans), T0])
        # Then we correct the Z-translation
        Tsurf = reduce(np.dot, [T1, self.myObject.Tcom, self.myObject.Tsurfaces[ifacet1]])
        Ttransz = utils.ComputeTTrans(2, self.myObject.Trest[2, 3] - Tsurf[2, 3])
        T1 = np.dot(Ttransz, T1)
        # Move T1 to the given nominal position
        qobj1 = self.myObject.ComputeQObject(T1)
        qobj1[1], qobj1[2] = nominalPosition
        T1 = self.myObject.ComputeTObject(qobj1)

        #
        # Correct T0 and T1 if needed
        #
        violated = False
        [lapproachingdirs, rapproachingdirs] = self.FindGraspCandidates(T1, qobj1)
        # Current heuristics: if at T1 there is at least one approaching direction which
        # is pointing toward -X-axis, we change the flipping motion to be flipping
        # 'forward' instead of backward. (It might be a bit too expensive to check all of
        # them but anyway, we will see.)
        with self.mobj:
            self.mobj.SetTransform(T1)
            for lapp in lapproachingdirs:
                # Find out what the approaching vector is
                linkIndex = int(lapp)/6
                realApp = np.mod(lapp, 6)
                # Tlink = self.mobj.GetLinks()[linkIndex].GetTransform()
                Tlink = np.dot(self.mobj.GetTransform(), self.myObject.graspinfos[linkIndex].Trel)
                approachingVect = utils.Normalize(Tlink[0:3, np.mod(realApp, 3)])
                if realApp > 2:
                    approachingVect *= -1

                # See if the approaching vector violates our condition(s).
                angle = np.arccos(np.dot(approachingVect, MX))
                if angle <= self._threshangle:
                    violated = True
                    break
        if not violated:
            with self.env:
                self.lrobot.SetActiveDOFValues(np.zeros(6))
                self.rrobot.SetActiveDOFValues(np.zeros(6))
                self.mobj.SetTransform(T0)
                violated = self.env.CheckCollision(self.mobj)
                if not violated:
                    self.mobj.SetTransform(T1)
                    violated = self.env.CheckCollision(self.mobj)
                    if not violated:
                        # No change needed
                        return [qobj0, qobj1], [T0, T1]

        # Now we change from flipping backward to flipping forward
        qobj0[-1] += np.pi
        qobj1[-1] += np.pi
        T0 = self.myObject.ComputeTObject(qobj0)
        T1 = self.myObject.ComputeTObject(qobj1)            
        
        return [qobj0, qobj1], [T0, T1]


    def PerturbPlacement(self, qobject, dx, dy, dtheta):
        """Generate another object transformation in the vicinity of the transformation specified
        by qobject. The magnitude of noise is specified by (dx, dy, dtheta)

        Parameters
        ----------
        qobject : list
        dx : float
        dy : float
        dtheta : float

        Returns
        -------
        qobjectNew : list
        Tnew : numpy.ndarray
        
        """
        ifacet, x, y, theta = qobject
        passed = False
        with self.mobj:
            self.lrobot.SetActiveDOFValues(np.zeros(6))
            self.rrobot.SetActiveDOFValues(np.zeros(6))
            while not passed:
                xNew = x + dx * rng.uniform(-1, 1)
                yNew = y + dy * rng.uniform(-1, 1)
                thetaNew = theta + dtheta * rng.uniform(-1, 1)
                qobjectNew = [ifacet, xNew, yNew, thetaNew]
                Tnew = self.myObject.ComputeTObject(qobjectNew)
                self.mobj.SetTransform(Tnew)
                passed = not self.env.CheckCollision(self.mobj)
        return qobjectNew, Tnew


    def SampleGrasps(self, T0, T1, qobj0, qobj1):
        """Given two transformations, sample a grasp, for each robot, which is likely to be
        connectible by a closed-chain motion.

        Returns
        -------
        qgrasps : list
            List of two qgrasp, one for each robot
        bestlsols : list
            List of two IK solutions for the left robot
        bestrsols : list
            List of two IK solutions for the right robot
        
        """
        ifacet0 = qobj0[0]        
        # Search for approaching direcions
        [lapproachingdirs0, rapproachingdirs0] = self.FindGraspCandidates(T0, qobj0)
        [lapproachingdirs1, rapproachingdirs1] = self.FindGraspCandidates(T1, qobj1)
        # Find intersection between the approaching directions available at the start and goal
        lapproachingdirs = [l for l in lapproachingdirs0 if l in lapproachingdirs1]
        rapproachingdirs = [r for r in rapproachingdirs0 if r in rapproachingdirs1]
        rng.shuffle(lapproachingdirs)
        rng.shuffle(rapproachingdirs)

        lFound = False
        rFound = False
        lApproachingDirection = None
        rApproachingDirection = None
        ntrials = 10 # numbe of trials for each approaching direction
        invalidGraspsAllowance = 5

        with self.env:
            self.lrobot.SetActiveDOFValues(self._homeconfig)
            self.rrobot.SetActiveDOFValues(self._homeconfig)
        log.debug("Start sampling grasps for left hand")
        for lapproachingdir in lapproachingdirs:
            isOK = (self.FilterApproachingDirection(lapproachingdir, T0, 0) and
                    self.FilterApproachingDirection(lapproachingdir, T1, 0))
            if not isOK:
                continue
            
            graspedLinkIndex = int(lapproachingdir)/6
            nInvalidGrasps = 0
            for itrial in xrange(ntrials):
                if nInvalidGrasps > invalidGraspsAllowance:
                    # log.info("nInvalidGrasps reachs the limit ({0} invalid grasps)".format(invalidGraspsAllowance))
                    break                
                qgrasp_l = self.myObject.SampleQGrasp(ifacet0, lapproachingdir)
                lsols0 = [] # available IKs at T0
                lsols1 = [] # available IKs at T1
                with self.env:
                    self.mobj.SetTransform(T0)
                Tgripper_l = self.myObject.ComputeTGripper(qgrasp_l)
                # Tgripper_l = utils.ComputeTGripper2(self.mobj, graspedLinkIndex, qgrasp_l)
                lsols0 = self.lmanip.FindIKSolutions(Tgripper_l, ikfilter)
                if len(lsols0) == 0:
                    # log.info("lrobot: no IK solution at T0")
                    continue
                with self.env:
                    self.mobj.SetTransform(T1)
                Tgripper_l = self.myObject.ComputeTGripper(qgrasp_l)
                # Tgripper_l = utils.ComputeTGripper2(self.mobj, graspedLinkIndex, qgrasp_l)
                lsols1 = self.lmanip.FindIKSolutions(Tgripper_l, ikfilter)
                if len(lsols1) == 0:
                    # log.info("lrobot: no IK solution at T1")
                    continue
                if not self.CheckGrasp(lsols0[0], T0, 0, qgrasp_l):
                    # log.info("lrobot: invalid grasp at T0")
                    nInvalidGrasps += 1
                    continue
                bestlsol0, bestlsol1 = self.FilterIKSolutions(lsols0, lsols1)
                if bestlsol0 is None:
                    continue
                lFound = True
                break
            if lFound:
                break
        if not lFound:
            return [], [], []

        log.debug("Start sampling grasps for right hand")
        for rapproachingdir in rapproachingdirs:
            isOK = (self.FilterApproachingDirection(rapproachingdir, T0, 1) and
                    self.FilterApproachingDirection(rapproachingdir, T1, 1))
            if not isOK:
                continue

            graspedLinkIndex = int(rapproachingdir)/6
            if graspedLinkIndex == int(lapproachingdir)/6:
                # We don't want the two robots to grasp the same link
                continue
            nInvalidGrasps = 0
            for itrial in xrange(ntrials):
                if nInvalidGrasps > invalidGraspsAllowance:
                    # log.info("nInvalidGrasps reachs the limit ({0} invalid grasps)".format(invalidGraspsAllowance))
                    break
                qgrasp_r = self.myObject.SampleQGrasp(ifacet0, rapproachingdir)
                # We need to prevent the two robots grasping close to each other
                if int(qgrasp_r[0])/6 == int(qgrasp_l[0])/6:
                    # Grasping the same link
                    if abs(abs(qgrasp_r[-1]) - abs(qgrasp_l[-1])) < 0.1:
                        # Two gripper should be far apart for more than 10 cm.
                        continue
                rsols0 = [] # available IKs at T0
                rsols1 = [] # available IKs at T1
                with self.env:
                    self.mobj.SetTransform(T0)
                    self.lrobot.SetActiveDOFValues(bestlsol0)
                Tgripper_r = self.myObject.ComputeTGripper(qgrasp_r)
                # Tgripper_r = utils.ComputeTGripper2(self.mobj, graspedLinkIndex, qgrasp_r)
                rsols0 = self.rmanip.FindIKSolutions(Tgripper_r, ikfilter)
                if len(rsols0) == 0:
                    # log.info("rrobot: no IK solution at T0")
                    continue
                with self.env:
                    self.mobj.SetTransform(T1)
                    self.lrobot.SetActiveDOFValues(bestlsol1)
                Tgripper_r = self.myObject.ComputeTGripper(qgrasp_r)
                # Tgripper_r = utils.ComputeTGripper2(self.mobj, graspedLinkIndex, qgrasp_r)
                rsols1 = self.rmanip.FindIKSolutions(Tgripper_r, ikfilter)
                if len(rsols1) == 0:
                    # log.info("rrobot: no IK solution at T1")
                    continue
                if not self.CheckGrasp(rsols0[0], T0, 1, qgrasp_r):
                    # log.info("rrobot: invalid grasp at T0")
                    nInvalidGrasps += 1
                    continue
                bestrsol0, bestrsol1 = self.FilterIKSolutions(rsols0, rsols1)
                if bestrsol0 is None:
                    continue
                rFound = True
                break
            if rFound:
                break
        if not rFound:
            return [], [], []

        log.info("SampleGrasps successful")
        return [qgrasp_l, qgrasp_r], [bestlsol0, bestlsol1], [bestrsol0, bestrsol1]        


    def FindGraspCandidates(self, Tobj, qobj):
        """Look for approaching directions (for both robots) which could possibly be grasps by the
        corresponding hand.

        Returns
        -------
        result : list
            List of two sublists, each one contains possible approaching directions for each hand.
        
        """
        ifacet = qobj[0]
        lapproachingdirs = []
        rapproachingdirs = []
        for boxinfo in self.myObject.graspinfos.values():
            for realapproachingdir in boxinfo.approachingDirections[ifacet]:
                approachingdir = 6*boxinfo.linkIndex + realapproachingdir
                lapproachingdirs.append(approachingdir)
                rapproachingdirs.append(approachingdir)
        # log.info("lapproachingdirs = {0}".format(lapproachingdirs))
        # log.info("rapproachingdirs = {0}".format(rapproachingdirs))
        return [lapproachingdirs, rapproachingdirs]


    def FilterIKSolutions(self, sols0, sols1, heuristicsIndex=0):
        """Find a pair of ik solutions (sol0, sol1), where sol0 \in sols0 and sol1 \in sols1,
        which is likely to lead to successful closed-chain query.

        """
        bestsol0 = None
        bestsol1 = None
        minDistance = 1e10

        if heuristicsIndex == 0:
            def CheckIKPair(q0, q1):
                if ((abs(q0[0] - q1[0]) <= np.pi) and
                    (q0[1] * q1[1] >= 0) and
                    (q0[2] * q1[2] >= 0) and
                    (q0[3] * q1[3] >= 0) and
                    (q0[4] * q1[4] >= 0)):
                    return True
                return False
        else:
            raise NotImplementedError, "Heuristics index = {0} not defined".format(heuristicsIndex)

        for (sol0, sol1) in itertools.product(sols0, sols1):
            # The first layer of filter is L2-distance
            dist = utils.Norm(sol0 - sol1)
            if dist > minDistance:
                continue
            minDistance = dist
            if CheckIKPair(sol0, sol1):
                bestsol0 = sol0
                bestsol1 = sol1
        # if bestsol0 is None and bestsol1 is None:
        #     log.info("IKs are not likely connectible by closed-chain motions")
        return bestsol0, bestsol1


    def FilterApproachingDirection(self, approachingdirection, Tobj, robotindex):
        """Use some heuristics to evaluate quickly if the given approaching direction will likely
        lead to any IK solution.

        The current heuristic used is that if the approaching direction is directed
        'towards' the robot, it is not likely that the robot will be able the approaching
        the object via this direction, hence eliminated.

        """
        robot = self.lrobot if robotindex == 0 else self.rrobot
        realapproachingdir = np.mod(approachingdirection, 6)
        graspedLinkIndex = int(approachingdirection)/6
        with self.mobj:
            self.mobj.SetTransform(Tobj)
            # Tlink = self.mobj.GetLinks()[graspedLinkIndex].GetTransform()
            Tlink = np.dot(self.mobj.GetTransform(), self.myObject.graspinfos[graspedLinkIndex].Trel)
        plink = utils.ExtractTranslationVector(Tlink)
        probot = utils.ExtractTranslationVector(robot.GetTransform())
        linkToRobotVect = utils.Normalize(plink - probot)
        # approachingVect is the approaching direction described in the world's frame
        approachingVect = Tlink[0:3, np.mod(realapproachingdir, 3)]
        if realapproachingdir > 2:
            approachingVect *= -1.0
            
        angle = np.arccos(np.dot(approachingVect, linkToRobotVect))
        if angle < self._threshangle:
            return True
        else:
            MZ = np.array([0., 0., -1.])
            angle = np.arccos(np.dot(MZ, approachingVect))
            if angle <= np.pi/3 and Tobj[2, 3] - self.myObject.Trest[2, 3] <= 0.5:
                return True
            return False


    def CheckGrasp(self, qrobot, Tobj, robotindex, qgrasp):
        """Given a pre-grasp configuration of a robot, check if the robot can correctly grasp the
        object.

        """
        graspingRobot = self.lrobot if robotindex == 0 else self.rrobot
        taskmanip = self.ltaskmanip if robotindex == 0 else self.rtaskmanip
        controller = graspingRobot.GetController()
        otherRobot = self.rrobot if robotindex == 0 else self.lrobot
        report = orpy.CollisionReport()
        isGrasping = False
        expectedLinkIndex = qgrasp[0]

        # Enable boxes and disable mesh
        linkEnabled = []
        for l in self.mobj.GetLinks():
            linkEnabled.append(l.IsEnabled())

            if l.GetGeometries()[0].GetType() == orpy.GeometryType.Box:
                l.Enable(True)
            elif l.GetGeometries()[0].GetType() == orpy.GeometryType.Trimesh:
                l.Enable(False)
        
        # If the robot can correctly grasp the object, the gripper should collide with
        # expectedLink.
        with self.env:
            self.mobj.SetTransform(Tobj)
            graspingRobot.SetActiveDOFValues(qrobot)
            taskmanip.CloseFingers()
            otherRobot.SetActiveDOFValues(self._homeconfig)
        while not controller.IsDone():
            time.sleep(self._controllersleeptime)
        if self.env.CheckCollision(graspingRobot, self.mobj, report=report):
            if report.plink1 in graspingRobot.GetLinks():
                graspedLink = report.plink2
            else:
                graspedLink = report.plink1
            # graspedLink is the object's link currently grasped by the graspingRobot
            expectedLink = self.mobj.GetLinks()[expectedLinkIndex]
            if expectedLink.GetName() == graspedLink.GetName():
                isGrasping = True
            else:
                # Since collision report can report only two links in collision. It might
                # be the case that the graspedLink and the expected are both grasped.
                graspedLink.Enable(False)
                if self.env.CheckCollision(graspingRobot, self.mobj, report=report):
                    if expectedLink in [report.plink1, report.plink2]:
                        isGrasping = True
                graspedLink.Enable(True)
        # Quickly ungrasp
        graspingRobot.SetDOFValues([0], [6])

        for wasEnabled, l in zip(linkEnabled, self.mobj.GetLinks()):
            l.Enable(wasEnabled)
        
        return isGrasping    


class OpenRAVEKinBodyWrapper(object):
    """A wrapper for an OpenRAVE KinBody. When calling SetTransform and GetTransform on the
    original KinBody, it will set and get the transform of the object's local frame, which
    can be at arbitrary position relative to the object. This class acts as another
    interface layer to the object such that SetTransform and GetTransform will set and get
    the transform of the frame attached to the object's COM instead.

    """    
    def __init__(self, mobj, TcomLocal):
        self.mobj = mobj
        self.Tcom = np.array(TcomLocal)
        self.TcomInv = np.linalg.inv(self.Tcom)


    def GetEnv(self):
        return self.mobj.GetEnv()

    
    def GetTransform(self):
        return np.dot(self.mobj.GetTransform(), self.Tcom)

    
    def SetTransform(self, T):
        self.mobj.SetTransform(np.dot(T, self.TcomInv))

    
