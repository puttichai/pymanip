import numpy as np
import cdd

NUMBER_TYPE = 'float'  # 'float' or 'fraction'

"""
In order to use this, you need to install cdd library:

sudo pip install pycddlib

"""

_epsilon = 1e-10

class ConeException(Exception):

    def __init__(self, M):
        self.M = M


class NotConeFace(ConeException):

    def __str__(self):
        return "Matrix is not a cone face"


class NotConeSpan(ConeException):

    def __str__(self):
        return "Matrix is not a cone span"


def SpanToFace(R, datatype=0, numtype=NUMBER_TYPE):
    """
    SpanToFace returns the face matrix A of the span matrix R, that
    is, a matrix such that

        {x = R z, z >= 0} if and only if {A x <= 0}.

    V-representation:
    first column elements -- 0 for ray
                             1 for vertex
    """
    V = np.hstack((np.tile(datatype, (R.shape[1], 1)), R.T))
    
    V_cdd = cdd.Matrix(V, number_type=numtype)
    V_cdd.rep_type = cdd.RepType.GENERATOR
    P = cdd.Polyhedron(V_cdd)
    ineq = P.get_inequalities()
    H = np.array(ineq)
    if H.shape == (0, ):
        return np.array([]), np.array([])
    A = []
    b = []
    if numtype == 'fraction':
        H = FractionToFloatArray(H)
    for i in xrange(H.shape[0]):
        # H matrix is [b, -A] for Ax <= b
        if np.linalg.norm(H[i, 1:]) < _epsilon:
            continue
        elif i not in ineq.lin_set:
            A.append(-H[i, 1:])
            b.append(H[i, 0])
    return np.array(A), np.array(b)


def FaceToSpan(A, b=None, numtype=NUMBER_TYPE):
    """
    Compute the span matrix F^S of the face matrix F,
    that is, a matrix such that

        {A x <= b} if and only if {x = R z, z >= 0}.

    H-representation:
    matrix: [b, A]
    """
    
    if b is None:
        b = np.zeros((A.shape[0], 1))
        
    H = np.hstack((np.reshape(b, (A.shape[0], 1)), -A))
        
    H_cdd = cdd.Matrix(H, number_type = numtype)
    H_cdd.rep_type = cdd.RepType.INEQUALITY
    P = cdd.Polyhedron(H_cdd)
    g = P.get_generators()
    V = np.array(g)
    vrep = []
    if numtype == 'fraction':
        V = FractionToFloatArray(V)
    for i in xrange(V.shape[0]):
        if V[i, 0] != 1:
            raise Exception("Polyhedron is not a convex polygon")
        if i not in g.lin_set:
            vrep.append(V[i, 1:])
    return np.array(vrep).T


FractionToFloatArray = np.vectorize(float)
