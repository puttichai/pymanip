import openravepy as orpy
import numpy as np
import scipy as sp
from scipy.spatial import ConvexHull
from copy import deepcopy
import time
import IPython
import trimesh

# CVXOPT
import cvxopt
import cvxopt.solvers
from cvxopt import matrix as cvxmat
from warnings import warn
try:
    import cvxopt.glpk
    __default_solver = 'glpk'
    # GLPK is the fastest LP solver I could find so far:
    # <https://scaron.info/blog/linear-programming-in-python-with-cvxopt.html>
    # ... however, it's verbose by default, so tell it to STFU:
    cvxopt.solvers.options['glpk'] = {'msg_lev': 'GLP_MSG_OFF'}  # cvxopt 1.1.8
    cvxopt.solvers.options['msg_lev'] = 'GLP_MSG_OFF'  # cvxopt 1.1.7
    cvxopt.solvers.options['LPX_K_MSGLEV'] = 0  # previous versions
except ImportError:
    __default_solver = None
cvxopt.solvers.options['show_progress'] = False  # disable cvxopt output


from . import utils

_qhull_prec = 'E0.001'

class ContactSet(object):

    def __init__(self, points, R):
        """A class containing a set of contact points on the same contact surface.

        contacts -- a list of contact points, each point is described in the world frame 

        R -- a rotation matrix describing a frame attached to the contact surface (w.r.t
        the world frame). The last column of R is the normal vector pointing into the
        surface.

        """
        self.contacts = deepcopy(points)
        self.R = np.array(R)
        self.N = len(self.contacts)
        self._G = None
        
    @property
    def G(self):
        """G is a grasp matrix.
        """
        if self._G is None:
            self.ComputeGraspMatrix()

        return self._G


    def ComputeGraspMatrix(self):
        G = []
        for point in self.contacts:
            G.append(np.vstack([np.eye(3), utils.ComputeSkewFromVect(point)]))
        self._G = np.hstack(G)


    def ComputeConstraints(self, mu, fmax=None):
        """Suppose fi is the contact force exerted at the contact point i. This function computes
        constraints on those contact forces, i.e.,friction constraints (and max grip force
        if aplicable).
        
        Return U, b. Valid contact forces must verify

        U*f <= b

        where f = [f1, f2, ..., fN], N is the number of contact points.
        
        """

        
        normal = self.R[0:3, 2]
        t1 = self.R[0:3, 0]
        t2 = self.R[0:3, 1]

        Upoint = np.vstack([-normal,
                            -t1 - mu*normal,
                             t1 - mu*normal,
                            -t2 - mu*normal,
                             t2 - mu*normal])
        U = [Upoint] * self.N
        Usurf = sp.linalg.block_diag(*U)
        b = np.zeros(5 * self.N)

        if fmax is not None:
            # Normal components of all contact forces must sum up to no greater than fmax.
            UlastRow = np.tile(normal, self.N)
            Usurf = np.vstack([Usurf, UlastRow])
            b = np.append(b, fmax)

        return Usurf, b
        
        
def GetContactPoints(robot, mobj, qgrasp, TcomLocal):
    """
    """

    lfingertipname = 'robotiq_fingertip_l'
    rfingertipname = 'robotiq_fingertip_r'
    
    # If the robot is already grabbing the object, need to ungrab first.
    isGrabbing = robot.IsGrabbing(mobj) is not None
    if isGrabbing:
        robot.Release(mobj)

    # Change the collision checker to pqp in order to be able to obtain contact points.
    lcolRep = orpy.CollisionReport()
    rcolRep = orpy.CollisionReport()
    env = robot.GetEnv()
    prevChecker = env.GetCollisionChecker()
    usingNewChecker = not (prevChecker.GetXMLId() == 'pqp')
    if usingNewChecker:
        env.SetCollisionChecker(orpy.RaveCreateCollisionChecker(env, 'pqp'))

    env.CheckCollision(robot.GetLink(lfingertipname), mobj, report=lcolRep)
    env.CheckCollision(robot.GetLink(rfingertipname), mobj, report=rcolRep)
    if usingNewChecker:
        env.SetCollisionChecker(prevChecker)

    # Extract all detected contact points
    lpoints = [c.pos for c in lcolRep.contacts]
    rpoints = [c.pos for c in rcolRep.contacts]
    if len(lpoints) == 0:
        raise Exception("No contact detected on the left fingertip")
    if len(rpoints) == 0:
        raise Exception("No contact detected on the right fingertip")

    # Restore the grabbing state
    if isGrabbing:
        robot.Grab(mobj)

    # Preprocess contact points
    approachingDir, slidingDir, delta = qgrasp[-3:] # unpack data
    realApproachingDir = np.mod(approachingDir, 6)
    ibox = int(approachingDir/6)
    link = mobj.GetLinks()[ibox]
    extents = link.GetGeometries()[0].GetBoxExtents()
    # grippingDir is 
    if np.mod(realApproachingDir, 3) == 0:
        # Approaching direction is +X/-X
        if np.mod(slidingDir, 3) == 1:
            extent = extents[2]
            grippingDir = 2
        elif np.mod(slidingDir, 3) == 2:
            extent = extents[1]
            grippingDir = 1
    elif np.mod(realApproachingDir, 3) == 1:
        # Approaching direction is +Y/-Y
        if np.mod(slidingDir, 3) == 0:
            extent = extents[2]
            grippingDir = 2
        elif np.mod(slidingDir, 3) == 2:
            extent = extents[0]
            grippingDir = 0
    elif np.mod(realApproachingDir, 3) == 2:
        # Approaching direction is +Y/-Y
        if np.mod(slidingDir, 3) == 0:
            extent = extents[1]
            grippingDir = 1
        elif np.mod(slidingDir, 3) == 1:
            extent = extents[0]
            grippingDir = 0

    # Create a convex hull of all contact points (for removing redundant points). We need only the
    # contact points that are vertices of the contact surface.

    # We first transform all points into the link's frame because in the link's frame, there will be
    # one coordinate which is (approximately) constant.
    Tlink = link.GetTransform()
    TlinkInv = np.linalg.inv(Tlink)
    lLinkLocalPoints = [utils.Transform(p, TlinkInv) for p in lpoints]
    rLinkLocalPoints = [utils.Transform(p, TlinkInv) for p in rpoints]

    # All the points (in the link's local frame) are coplanar. The plane is actuall described by
    # grippingDir. Due to some numerical error, we compute the average of that *constant*
    # coordinate.    
    lAvgCoord = np.average([p[grippingDir] for p in lLinkLocalPoints])
    rAvgCoord = np.average([p[grippingDir] for p in rLinkLocalPoints])

    # Process the left contact surface
    lLinkLocalPoints2d = [np.asarray([p[i] for i in xrange(3) if not (i == grippingDir)]) for p in lLinkLocalPoints]
    lHull = ConvexHull(lLinkLocalPoints2d, qhull_options=_qhull_prec)
    lHullLocalPoints2d = [lHull.points[i].tolist() for i in lHull.vertices] # a set of non-redundant points
    lHullLocalPoints = deepcopy(lHullLocalPoints2d)
    for point in lHullLocalPoints:
        point.insert(grippingDir, lAvgCoord)
    lHullLocalPoints = [np.asarray(p) for p in lHullLocalPoints]
    
    # Process the right contact surface
    rLinkLocalPoints2d = [np.asarray([p[i] for i in xrange(3) if not (i == grippingDir)]) for p in rLinkLocalPoints]
    rHull = ConvexHull(rLinkLocalPoints2d, qhull_options=_qhull_prec)
    rHullLocalPoints2d = [rHull.points[i].tolist() for i in rHull.vertices] # a set of non-redundant points
    rHullLocalPoints = deepcopy(rHullLocalPoints2d)
    for point in rHullLocalPoints:
        point.insert(grippingDir, rAvgCoord)
    rHullLocalPoints = [np.asarray(p) for p in rHullLocalPoints]

    # Now transform all filtered points back into the global frame
    lPoints = [utils.Transform(p, Tlink) for p in lHullLocalPoints]
    rPoints = [utils.Transform(p, Tlink) for p in rHullLocalPoints]

    # Now we compute the description of the contact surface's frame (= obtain normal and tangential
    # components). We make use of the information of the gripper's transformation.
    Tmanip = robot.GetActiveManipulator().GetTransform()
    Rmanip = utils.ExtractRotationMatrix(Tmanip)

    # Rl is the rotation matrix describing the contact surface's frame of the surface touchuing the
    # gripper's left finger tip. The last column represent the normal vector of the surface's frame.
    # Since we are considering the free body diagram of the object, the normal vector should be
    # pointing *inward* (toward the object).

    # The following calculations are based on the current gripper configuration (denso + robotiq
    # gripper). If the robot changes, we also need to modify this.
    Rl = np.eye(3)
    Rl[0:3, 0] = Rmanip[0:3, 0]
    Rl[0:3, 1] = -Rmanip[0:3, 2]
    Rl[0:3, 2] = Rmanip[0:3, 1]

    Rr = np.eye(3)
    Rr[0:3, 0] = -Rmanip[0:3, 0]
    Rr[0:3, 1] = -Rmanip[0:3, 2]
    Rr[0:3, 2] = -Rmanip[0:3, 1]

    # Now lPoints, rPoints, Rl, Rr are all described in the global frame.
    contactSet_left = ContactSet(lPoints, Rl)
    contactSet_right = ContactSet(rPoints, Rr)
    return contactSet_left, contactSet_right


def CheckStaticEquilibrium(robots, qgrasps, mobj, Tobj, Trest, fmax, mu, myObject, otherContactPoints=[], checkSingleRobot=False):
    """
    """
    
    TcomLocal = myObject.Tcom
    Tcom = np.dot(Tobj, TcomLocal)
    pcom = np.array(Tcom[0:3, 3])

    # Calculate the object's mass
    env = robots[0].GetEnv()
    g = env.GetPhysicsEngine().GetGravity()
    m = 0
    for link in mobj.GetLinks():
        m += link.GetMass()

    # Here WGi is defined such that WGI = Gf.
    WGI = -np.hstack([m*g, m*np.cross(pcom, g)])

    result = dict()
    U_all = []
    b_all = []
    G_all = []
    # Deal with placement contact points
    contactSet_placement = ContactSet(otherContactPoints, Trest[0:3, 0:3])
    G_placement = contactSet_placement.G
    U_placement, b_placement = contactSet_placement.ComputeConstraints(mu, None)
    G_all.append(G_placement)
    U_all.append(U_placement)
    b_all.append(b_placement)
    
    # Deal with contact points from grasps
    for (irobot, (robot, qgrasp)) in enumerate(zip(robots, qgrasps)):
        contactSet_left, contactSet_right = GetContactPoints(robot, mobj, qgrasp, TcomLocal=TcomLocal)
        G_l = contactSet_left.G
        G_r = contactSet_right.G
        Usurf_l, b_l = contactSet_left.ComputeConstraints(mu, fmax)
        Usurf_r, b_r = contactSet_right.ComputeConstraints(mu, fmax)

        G = [G_l, G_r]
        U = [Usurf_l, Usurf_r]
        b = [b_l, b_r]
        
        if checkSingleRobot:
            # import IPython; IPython.embed()
            G.append(G_placement)
            U.append(U_placement)
            b.append(b_placement)

            G_ = np.hstack(G)
            U_ = sp.linalg.block_diag(*U)
            b_ = np.hstack(b)
            c = np.zeros(U_.shape[1])
            sol = cvxopt.solvers.lp(cvxmat(c), cvxmat(U_), cvxmat(b_), cvxmat(G_), cvxmat(WGI))
            result[irobot] = ('optimal' in sol['status'])

        G_all.append(G_l)
        G_all.append(G_r)
        U_all.append(Usurf_l)
        U_all.append(Usurf_r)
        b_all.append(b_l)
        b_all.append(b_r)

    if checkSingleRobot:
        return result
    
    G_ = np.hstack(G_all)
    U_ = sp.linalg.block_diag(U_all)
    b_ = np.hstack(b_all)
    c = np.zeros(U_.shape[1])
    sol = cvxopt.solvers.lp(cvxmat(c), cvxmat(U_), cvxmat(b_), cvxmat(G_), cvxmat(WGI))
    result['all'] = ('optimal' in sol['status'])
    return result
