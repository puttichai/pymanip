import openravepy as orpy
import numpy as np
import time
import IPython
import random
rng = random.SystemRandom()

from bimanual.planners import cc_planner as ccp
from pymanip.planningutils import utils
from pymanip.planningutils import myobject

"""
Test the issuer on computing certificates
"""

availableObjects = dict()
availableObjects[len(availableObjects.keys())] = "ikea_right_frame.kinbody.xml"
availableObjects[len(availableObjects.keys())] = "rectangularcage.xml"
availableObjects[len(availableObjects.keys())] = "testobject1.kinbody.xml"
availableObjects[len(availableObjects.keys())] = "chair.xml"
availableObjects[len(availableObjects.keys())] = "ikea-stefan.xml"
availableObjects[len(availableObjects.keys())] = "taburet1.xml"
availableObjects[len(availableObjects.keys())] = "taburet2.xml"
availableObjects[len(availableObjects.keys())] = "square.xml"
availableObjects[len(availableObjects.keys())] = "praktrik2.xml"
availableObjects[len(availableObjects.keys())] = "praktrik3.xml"
availableObjects[len(availableObjects.keys())] = "forte.xml"
msg = "Choose an object to be manipulated:"
for (key, val) in availableObjects.iteritems():
    msg += "\n{0} : {1}".format(key, val)
msg += "\n\n>> "
while True:
    try:
        index = int(raw_input(msg))
        if index in availableObjects.keys():
            break
    except:
        pass        

## Environment initialization
# viewername = 'qtcoin'
viewername = 'qtosg'
collisioncheckername = 'fcl_'
env = orpy.Environment()
env.SetViewer(viewername)
vw = env.GetViewer()
cc = orpy.RaveCreateCollisionChecker(env, collisioncheckername)
cc.SetCollisionOptions(orpy.CollisionOptions.ActiveDOFs)
env.SetCollisionChecker(cc)

# Robots
from os import path
robotpath = path.expanduser('~') + '/git/bimanual/xml/robots/'
robotfilename = 'denso_ft_gripper_with_base.robot.xml'
manipulatorname = 'denso_ft_sensor_gripper'
lrobot = env.ReadRobotXMLFile(robotpath + robotfilename)
rrobot = env.ReadRobotXMLFile(robotpath + robotfilename)
lrobot.SetName('lrobot')
rrobot.SetName('rrobot')
env.Add(lrobot)
env.Add(rrobot)
# Relative transformation of the right_robot w.r.t the left_robot. (from calibration)
Tleft_right = \
np.array([[ 0.9999599222893029,   -0.0040494323330103115, 0.007142558327789952, -0.001071647451604687],
          [ 0.004003843532690571,  0.999975285228445,     0.005714046662232913,  1.0708242599889104  ],
          [-0.008007687065380032, -0.0056852199703562875, 0.9999581658905917,   -0.003974407767300958],
          [ 0.0,                   0.0,                   0.0,                   1.0                 ]])
rrobot.SetTransform(Tleft_right)

# Supporting surface
modelspath = '../pymanip/models/'
supporting_surface = env.ReadKinBodyXMLFile(modelspath + 'supporting_surface.kinbody.xml')
env.Add(supporting_surface)
Trest = np.array([[ 1.,  0.,  0.,  0.5*Tleft_right[0, 3]],
                  [ 0.,  1.,  0.,  0.5*Tleft_right[1, 3]],
                  [ 0.,  0.,  1., -0.155],
                  [ 0.,  0.,  0.,  1.]])
supporting_surface.SetTransform(Trest)
surface_frame = utils.CreateReferenceFrameKinBody(env, T=Trest, h=0.2, r=0.005)

# Movable object
mobj = env.ReadKinBodyXMLFile(modelspath + availableObjects[index])
env.Add(mobj)

## Setting up manipulators
ik6d = orpy.IkParameterization.Type.Transform6D
lmanip = lrobot.SetActiveManipulator(manipulatorname)
rmanip = rrobot.SetActiveManipulator(manipulatorname)
ltaskmanip = orpy.interfaces.TaskManipulation(lrobot)
rtaskmanip = orpy.interfaces.TaskManipulation(rrobot)
lcontroller = lrobot.GetController()
rcontroller = rrobot.GetController()
ik6d = orpy.IkParameterization.Type.Transform6D
likmodel = orpy.databases.inversekinematics.InverseKinematicsModel(lrobot, iktype=ik6d)
if not likmodel.load():
    raise Exception("Cannot load ikmodel for left-robot")
rikmodel = orpy.databases.inversekinematics.InverseKinematicsModel(rrobot, iktype=ik6d)
if not rikmodel.load():
    raise Exception("Cannot load ikmodel for right-robot")

## Initializing MyObject
myObject = myobject.MyObject(mobj)
myObject.SetRestingSurfaceTransform(Trest)
import threading
frame = utils.CreateReferenceFrameKinBody(env)
thread = threading.Thread(target=utils.AttachFrameToBody, args=[frame, mobj, myObject.Tcom])
thread.start()

##########################################################################################

from pymanip.planners import issuer
planner = issuer.Issuer([lrobot, rrobot], mobj, myObject)
ts = time.time()
result = planner.ComputeCertificate(np.array([0.25, 0]), True)
te = time.time()
print "Verification result: {0}".format('passed' if result else 'not passed')
print "verification time = {0}".format(te - ts)
