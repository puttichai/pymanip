import openravepy as orpy
import numpy as np
import time
import IPython
import random
from itertools import product
rng = random.SystemRandom()

from bimanual.planners import cc_planner as ccp
from pymanip.planningutils import utils, myobject
ikfilter = orpy.IkFilterOptions.CheckEnvCollisions

"""
Verifying connectedness of sets T_i^k.
"""
# Environment initialization
viewername = 'qtosg'
collisioncheckername = 'fcl_'
env = orpy.Environment()
# env.SetViewer(viewername)
# vw = env.GetViewer()
cc = orpy.RaveCreateCollisionChecker(env, collisioncheckername)
cc.SetCollisionOptions(orpy.CollisionOptions.ActiveDOFs)
env.SetCollisionChecker(cc)

# Robots
from os import path
robotpath = path.expanduser('~') + '/git/bimanual/xml/robots/'
robotfilename = 'denso_ft_gripper_with_base.robot.xml'
manipulatorname = 'denso_ft_sensor_gripper'
lrobot = env.ReadRobotXMLFile(robotpath + robotfilename)
rrobot = env.ReadRobotXMLFile(robotpath + robotfilename)
lrobot.SetName('lrobot')
rrobot.SetName('rrobot')
env.Add(lrobot)
env.Add(rrobot)
lrobot.SetActiveDOFs(xrange(6))
rrobot.SetActiveDOFs(xrange(6))

Tleft = utils.ComputeTRot(2, np.pi/2)
Tright = utils.ComputeTRot(2, -np.pi/2)
p = np.array([0.003, 1.078, 0.002])
Tright[0:3, 3] += p
lrobot.SetTransform(Tleft)
rrobot.SetTransform(Tright)

# Supporting surface
modelspath = '../pymanip/models/'
supporting_surface = env.ReadKinBodyXMLFile(modelspath + 'supporting_surface.kinbody.xml')
env.Add(supporting_surface)
Trest = np.array([[ 1.,  0.,  0.,  0.5*p[0]],
                  [ 0.,  1.,  0.,  0.5*p[1]],
                  [ 0.,  0.,  1., -0.155],
                  [ 0.,  0.,  0.,  1.]])
supporting_surface.SetTransform(Trest)
surface_frame = utils.CreateReferenceFrameKinBody(env, T=Trest, h=0.2, r=0.005)

# Manipulated object
mobj = env.ReadKinBodyXMLFile(modelspath + 'ikea-stefan.xml')
env.Add(mobj)

## Setting up manipulators
ik6d = orpy.IkParameterization.Type.Transform6D
lmanip = lrobot.SetActiveManipulator(manipulatorname)
rmanip = rrobot.SetActiveManipulator(manipulatorname)
ltaskmanip = orpy.interfaces.TaskManipulation(lrobot)
rtaskmanip = orpy.interfaces.TaskManipulation(rrobot)
lcontroller = lrobot.GetController()
rcontroller = rrobot.GetController()
ik6d = orpy.IkParameterization.Type.Transform6D
likmodel = orpy.databases.inversekinematics.InverseKinematicsModel(lrobot, iktype=ik6d)
if not likmodel.load():
    raise Exception("Cannot load ikmodel for left-robot")
rikmodel = orpy.databases.inversekinematics.InverseKinematicsModel(rrobot, iktype=ik6d)
if not rikmodel.load():
    raise Exception("Cannot load ikmodel for right-robot")

## Initializing MyObject
myObject = myobject.MyObject(mobj)
myObject.SetRestingSurfaceTransform(Trest)
# import threading
# frame = utils.CreateReferenceFrameKinBody(env)
# thread = threading.Thread(target=utils.AttachFrameToBody, args=[frame, mobj, myObject.Tcom])
# thread.start()

##########################################################################################

home = np.zeros(6)

def CollectApproachingDirections(myObject, ifacet):
    """Return a list of approaching directions available for the current placement class.

    """
    approachingdirs = []
    for boxinfo in myObject.graspinfos.values():
        for realapproachingdir in boxinfo.approachingDirections[ifacet]:
            approachingdir = 6*boxinfo.linkIndex + realapproachingdir
            approachingdirs.append(approachingdir)
    return approachingdirs

def IsReachable(myObject, qobj, approachingdirs):
    """Check if the object is reachable by both robots when it is placed at qobj.

    """
    # print "qobj = {0}".format(qobj)
    lFound = False
    rFound = False
    lsol = None
    rsol = None
    ntrials = 100 # numbe of trials for each approaching direction
    invalidGraspsAllowance = 10

    Tobj = myObject.ComputeTObject(qobj)
    
    lrobot.SetActiveDOFValues(home)
    rrobot.SetActiveDOFValues(home)
    # print "    Start sampling grasps for left robot"
    for appdir in approachingdirs:
        if not FilterApproachingDirection(appdir, Tobj, 0):
            # This approaching direction is not likely to be reachable
            continue
        graspedLinkIndex = int(appdir)/6
        nInvalidGrasps = 0
        for itrial in xrange(ntrials):
            if nInvalidGrasps > invalidGraspsAllowance:
                break
            lqgrasp = myObject.SampleQGrasp(ifacet, appdir)
            lTgripper = utils.ComputeTGripper2(mobj, graspedLinkIndex, lqgrasp)
            lsol = lmanip.FindIKSolution(lTgripper, ikfilter)
            if lsol is not None:
                lFound = True
                with env:
                    lrobot.SetActiveDOFValues(lsol)
                break
    if not lFound:
        return False
    
    # print "    Start sampling grasps for right robot"
    for appdir in approachingdirs:
        if not FilterApproachingDirection(appdir, Tobj, 1):
            # This approaching direction is not likely to be reachable
            continue
        graspedLinkIndex = int(appdir)/6
        nInvalidGrasps = 0
        for itrial in xrange(ntrials):
            if nInvalidGrasps > invalidGraspsAllowance:
                break
            rqgrasp = myObject.SampleQGrasp(ifacet, appdir)
            rTgripper = utils.ComputeTGripper2(mobj, graspedLinkIndex, rqgrasp)
            rsol = rmanip.FindIKSolution(rTgripper, ikfilter)
            if rsol is not None:
                rFound = True
                with env:
                    rrobot.SetActiveDOFValues(rsol)
                break
    if not rFound:
        return False
    
    return True


def FilterApproachingDirection(approachingdir, Tobj, robotindex):
    robot = lrobot if robotindex == 0 else rrobot
    realapproachingdir = np.mod(approachingdir, 6)
    graspedLinkIndex = int(approachingdir)/6
    mobj.SetTransform(Tobj)
    Tlink = mobj.GetLinks()[graspedLinkIndex].GetTransform()
    plink = utils.ExtractTranslationVector(Tlink)
    probot = utils.ExtractTranslationVector(robot.GetTransform())
    linkToRobotVect = utils.Normalize(plink - probot)
    approachingVect = Tlink[0:3, np.mod(realapproachingdir, 3)]
    if realapproachingdir > 2:
        approachingVect *= -1.0
    angle = np.arccos(np.dot(approachingVect, linkToRobotVect))
    if angle < np.pi * 0.5:
        return True
    else:
        MZ = np.array([0., 0., -1.])
        angle = np.arccos(np.dot(MZ, approachingVect))
        if angle <= np.pi/3 and Tobj[2, 3] - myObject.Trest[2, 3] <= 0.5:
            return True
        return False


import sys
def CheckAllAngles(X, Y, ifacet, myObject, step=3):
    """
    Parameters
    ----------
    step : int
        Rotation step (in degree)
    """
    results = []
    approachingdirs = CollectApproachingDirections(myObject, ifacet)
    N = len(X) * len(Y)
    coordinates = product(X, Y)

    Ntheta = 360 / step

    for index, (x, y) in enumerate(coordinates):
        msg = "checking coordinate {0:03d} (total {1}): ".format(index + 1, N)
        result_thetas = [] # all results at the current (x, y)
        for mult in range(360):
            if not (mult % step == 0):
                continue

            sys.stdout.write(msg + "theta {0:03d}/{1}\r".format((mult / step) + 1, Ntheta))
            sys.stdout.flush()
            
            qobj = [ifacet, x, y, mult*np.pi/180.]
            Tobj = myObject.ComputeTObject(qobj)
            with env:
                lrobot.SetActiveDOFValues(home)
                rrobot.SetActiveDOFValues(home)            
                mobj.SetTransform(Tobj)
            if env.CheckCollision(mobj):
                # This placement is not collision-free. Proceed to the next.
                result = [x, y, mult, False]
            else:
                result = [x, y, mult, IsReachable(myObject, qobj, approachingdirs)]
            result_thetas.append(result)
        results.append(result_thetas)
        # with open('assumption1.newenv.data', 'wb') as f:
        #     pickle.dump(results, f, pickle.HIGHEST_PROTOCOL)
        # print "saved the result for (x, y) = ({0}, {1})".format(x, y)
        
        # print "finish {0}/{1}: (x, y) = ({2}, {3})\r".format(index, N, x, y)
    return results


##########################################################################################
lrobot.SetActiveDOFValues(home)
rrobot.SetActiveDOFValues(home)
ifacet = 28


# import pickle
# with open('temp.pkl', 'r') as f:
#     results = pickle.load(f)

# set1
# X1 = np.arange(-0.40, 0.39, 0.02)
X1 = np.arange(-0.14, 0.39, 0.02)
Y1 = np.arange(-0.11, 0.00, 0.02)
# set2
X2 = np.arange( 0.40, 0.61, 0.02)
Y2 = np.arange(-0.71, 0.00, 0.02)
# Checking: since the supporting surface is symmetric (around x-axis), we check at only
# coordinates with negative y. Then from the symmetry, the results for positive y should
# be identical.
results1 = CheckAllAngles(X1, Y1, ifacet, myObject)
results2 = CheckAllAngles(X2, Y2, ifacet, myObject)
# set 3
X3 = np.arange(-0.50, -0.41, 0.02)
Y3 = np.arange(-0.11, 0.00, 0.02)
results3 = CheckAllAngles(X3, Y3, ifacet, myObject)
results = results1 + results2 + results3


# Plotting
handles = []
allPoints = [] # all unreachable points or reachable points, depending on plotGreen
plotGreen = 1 # true if plotting reachable points
pointSize = 3
xdir = Trest[0:3, 0]
ydir = Trest[0:3, 1]
zdir = Trest[0:3, 2]*0.02
handles = []
for result_thetas in results:
    x, y = result_thetas[0][0:2]
    if x < -0.11:
        continue
    # base point
    b1 = Trest[0:3, 3] + x*xdir + y*ydir
    b2 = Trest[0:3, 3] + x*xdir - y*ydir
    for (i, result) in enumerate(result_thetas):
        p1 = b1 + i*zdir
        p2 = b2 + i*zdir

        if plotGreen:
            if result[3]:
                color = [0, 1, 0]
            else:
                continue
        else:
            if result[3]:
                continue
            else:
                color = [1, 0, 0] # red
        handles.append(env.plot3(points=p1, pointsize=pointSize, colors=color))
        handles.append(env.plot3(points=p2, pointsize=pointSize, colors=color))
        allPoints.append(p1)
        allPoints.append(p2)

"""
# Write the (feasible) coordinates as vertices (off format)

data = "OFF"
data += "\n{0} 0 0".format(len(allPoints))
for point in allPoints:
    data += "\n{0} {1} {2}".format(*point)

with open('data/connectedness_data.off', 'w') as f:
    f.write(data)

# Set camera
vw = env.GetViewer()
ct = np.array([[-0.35154308, -0.03453421,  0.93553453, -2.0968399 ],
               [-0.93616421,  0.01696636, -0.3511534 ,  1.41800284],
               [-0.00374581, -0.99925949, -0.03829409,  0.61467487],
               [ 0.        ,  0.        ,  0.        ,  1.        ]])
vw.SetCamera(ct)

# Object pose for displaying
qobj = [28, 0.15251922584488464, -0.004884996569731667, 0.606137983951248]
mobj.SetTransform(myObject.ComputeTObject(qobj))


# Connectedness result
# Important note: incorrect separated connected components were removed. 
# They are wrong because they came from configurations where a robot base's leg is
# *inside* the frame of the back of the seat

env.Load('data/connectedness_constructed.off')
connectedness = env.GetBodies()[-1]
color = [0, 0.83, 0.45]
connectedness.GetLinks()[0].GetGeometries()[0].SetDiffuseColor(color)



# Heuristic display
cameraTransform = np.array([[-0.59398085,  0.0972825 , -0.79857552,  1.86335552],
       [ 0.80426658,  0.04898918, -0.592246  ,  1.77324772],
       [-0.01849361, -0.99405039, -0.10733969,  0.7115103 ],
       [ 0.        ,  0.        ,  0.        ,  1.        ]])

qobj1 = [28, 0.15251922584488464, -0.004884996569731667, -5.677047323228338]
qobj2 = [257, 0.20, 0, np.pi]

T1 = myObject.ComputeTObject(qobj1)
T2 = myObject.ComputeTObject(qobj2)

# draw the center line
p1 = np.array([ 10.0, p[1]/2, -0.155])
p2 = np.array([-10.0, p[1]/2, -0.155])
points = np.vstack([p1, p2])
h = env.drawlinestrip(points=points, linewidth=3, colors=color)

"""
