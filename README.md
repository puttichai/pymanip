pymanip
=======

A manipulation planning library for manipulators with parallel jaw grippers

Dependencies
============
* [OpenRAVE](https://github.com/rdiankov/openrave/tree/production)

* python [`bimanual`](https://github.com/quangounet/bimanual/tree/pymanip) package (`pymanip` branch)

* python [TOPP](http://github.com/quangounet/topp) package for time-optimal path parameterization

* python `trimesh` package
```
sudo pip install trimesh[all]
```

* python cdd library for cone double-description computation
```
sudo apt-get install pycddlib
```

* python `networkx` package
```
sudo pip install networkx
```

* python `cvxopt` package + GLPK solver
```
sudo apt-get install libglpk-dev
sudo CVXOPT_BUILD_GLPK=1 pip install cvxopt
```